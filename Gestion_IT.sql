CREATE DATABASE  IF NOT EXISTS `gestion_it` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `gestion_it`;
CREATE USER 'GestITAdmin'@'localhost' IDENTIFIED BY 'tgvfgh';
GRANT ALL PRIVILEGES ON gestion_it.* TO 'GestITAdmin'@'localhost' WITH GRANT OPTION;
FLUSH PRIVILEGES;

-- MySQL dump 10.13  Distrib 5.6.11, for Win64 (x86_64)
--
-- Host: localhost    Database: gestion_it
-- ------------------------------------------------------
-- Server version	5.6.11

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `classetechnique`
--

DROP TABLE IF EXISTS `classetechnique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classetechnique` (
  `IDClasse` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) NOT NULL,
  PRIMARY KEY (`IDClasse`),
  UNIQUE KEY `IDClasse_UNIQUE` (`IDClasse`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classetechnique`
--

LOCK TABLES `classetechnique` WRITE;
/*!40000 ALTER TABLE `classetechnique` DISABLE KEYS */;
INSERT INTO `classetechnique` VALUES (1,'Laptop'),(2,'Desktop'),(3,'Serveur'),(4,'Moniteur'),(5,'Imprimante');
/*!40000 ALTER TABLE `classetechnique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comp_disque`
--

DROP TABLE IF EXISTS `comp_disque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comp_disque` (
  `IDDisque` int(11) NOT NULL AUTO_INCREMENT,
  `Chemin` varchar(255) DEFAULT NULL,
  `Capacite` int(11) NOT NULL,
  `IDComposant` int(11) NOT NULL,
  PRIMARY KEY (`IDDisque`),
  UNIQUE KEY `IDDisque_UNIQUE` (`IDDisque`),
  KEY `IDCompDisque_idx` (`IDComposant`),
  CONSTRAINT `IDCompDisque` FOREIGN KEY (`IDComposant`) REFERENCES `composant` (`IDComposant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comp_disque`
--

LOCK TABLES `comp_disque` WRITE;
/*!40000 ALTER TABLE `comp_disque` DISABLE KEYS */;
INSERT INTO `comp_disque` VALUES (1,'C:',250,1),(2,'/dev/disk0s2',150,3),(3,'/dev/disk0s2',120,4),(4,'C:',350,5),(5,'C:',350,6),(6,'/dev/disk0s2',150,7),(7,'C:',350,8),(8,'C:',350,9),(9,'C:',250,10),(10,'C:',250,11),(11,'C:',350,12),(12,'C:',350,14),(13,'C:',150,15),(14,'C:',250,16),(15,'C:',300,17),(16,'C:',500,19),(17,'C:',500,20),(18,'C:',250,22),(19,'C:',250,23),(20,'/dev/disk0s2',300,25),(21,'C:',500,26),(22,'D:',4096,26),(23,'C:',500,27),(24,'D:',1024,27),(25,'C:',300,28),(26,'C:',300,29),(28,'C:',250,30),(29,'C:',500,31),(30,'C:',150,32),(31,'C:',300,33),(32,'/dev/disk0s2',1024,34),(33,'/dev/disk0s2',1024,35),(34,'C:',250,36),(35,'C:',500,37),(36,'/dev/disk0s2',250,38),(37,'C:',500,39),(38,'C:',300,40),(39,'C:',300,41),(40,'C:',300,42),(41,'C:',300,43),(42,'C:',300,44),(43,'C:',300,45),(44,'C:',300,46),(45,'C:',300,47),(46,'C:',300,48),(47,'C:',300,49),(48,'C:',300,50),(49,'C:',300,51),(50,'C:',300,52),(51,'C:',300,53),(52,'C:',300,54),(53,'C:',300,55),(54,'C:',300,56);
/*!40000 ALTER TABLE `comp_disque` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comp_reseau`
--

DROP TABLE IF EXISTS `comp_reseau`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comp_reseau` (
  `IDReseau` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle` varchar(255) NOT NULL,
  `IP` varchar(255) NOT NULL,
  `IDComposant` int(11) NOT NULL,
  PRIMARY KEY (`IDReseau`),
  UNIQUE KEY `IDReseau_UNIQUE` (`IDReseau`),
  KEY `IDCompReseau_idx` (`IDComposant`),
  CONSTRAINT `IDCompReseau` FOREIGN KEY (`IDComposant`) REFERENCES `composant` (`IDComposant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=87 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comp_reseau`
--

LOCK TABLES `comp_reseau` WRITE;
/*!40000 ALTER TABLE `comp_reseau` DISABLE KEYS */;
INSERT INTO `comp_reseau` VALUES (1,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.20',1),(2,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.21',1),(3,'en0','192.168.10.22',3),(4,'en1','192.168.10.23',3),(5,'en0','192.168.10.24',4),(6,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.25',5),(7,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.26',6),(8,'en0','192.168.10.27',7),(9,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.28',8),(10,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.29',9),(11,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.30',10),(12,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.31',10),(13,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.32',11),(14,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.33',11),(15,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.34',12),(16,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.35',14),(17,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.36',15),(18,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.37',16),(19,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.38',16),(20,'en0','192.168.10.39',17),(21,'en1','192.168.10.40',17),(22,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.41',19),(23,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.42',20),(24,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.43',22),(25,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.44',22),(26,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.45',23),(27,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.46',25),(28,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.47',26),(29,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.48',26),(30,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.49',26),(31,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.50',26),(32,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.51',27),(33,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.52',27),(34,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.53',28),(35,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.54',28),(36,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.55',29),(37,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.56',29),(38,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.57',30),(39,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.58',30),(40,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.59',31),(41,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.60',32),(42,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.61',33),(43,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.62',33),(44,'en0','192.168.10.63',34),(45,'en1','192.168.10.64',34),(46,'en0','192.168.10.65',35),(47,'en1','192.168.10.66',35),(48,'Intel Centrino Ultimate-N 6300 AGN','192.168.10.67',36),(49,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.68',36),(50,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.69',37),(51,'en0','192.168.10.70',38),(52,'Broadcom NetXtreme 57xx Gigabit Controller','192.168.10.71',39),(53,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.72',40),(54,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.73',40),(55,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.74',41),(56,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.75',41),(57,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.76',42),(58,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.77',42),(59,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.78',43),(60,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.79',43),(61,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.80',44),(62,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.81',44),(63,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.82',45),(64,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.83',45),(65,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.84',46),(66,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.85',46),(67,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.86',47),(68,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.87',47),(69,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.88',48),(70,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.89',48),(71,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.90',49),(72,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.91',49),(73,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.92',50),(74,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.93',50),(75,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.94',51),(76,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.95',51),(77,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.96',52),(78,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.97',52),(79,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.98',53),(80,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.99',43),(81,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.100',54),(82,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.101',54),(83,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.102',55),(84,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.103',55),(85,'Intel PRO/Wireless 3945ABG Network Connection','192.168.10.104',56),(86,'Intel 82566DM-2 Gigabit Network Connection','192.168.10.105',56);
/*!40000 ALTER TABLE `comp_reseau` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `composant`
--

DROP TABLE IF EXISTS `composant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `composant` (
  `IDComposant` int(11) NOT NULL AUTO_INCREMENT,
  `OS` varchar(255) DEFAULT NULL,
  `Memoire` int(11) DEFAULT NULL,
  `CPUNb` int(11) DEFAULT NULL,
  `CPUType` varchar(255) DEFAULT NULL,
  `CPUFrequ` float(3,2) DEFAULT NULL,
  `BIOSVer` varchar(255) DEFAULT NULL,
  `NoInventaire` int(11) NOT NULL,
  PRIMARY KEY (`IDComposant`),
  UNIQUE KEY `IDComposant_UNIQUE` (`IDComposant`),
  UNIQUE KEY `NoInventaire_UNIQUE` (`NoInventaire`),
  CONSTRAINT `NoInvComposant` FOREIGN KEY (`NoInventaire`) REFERENCES `equipement` (`NoInventaire`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=57 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `composant`
--

LOCK TABLES `composant` WRITE;
/*!40000 ALTER TABLE `composant` DISABLE KEYS */;
INSERT INTO `composant` VALUES (1,'Windows 7',4096,1,'Intel Core 2 Duo P9500',2.53,'A02',1),(2,NULL,128,NULL,NULL,NULL,NULL,2),(3,'Mac OS 10.7',4096,1,'Intel Core i7',2.00,'MBP22.88Z.00A5.B07.0708131242',3),(4,'Mac OS 10.6',4096,1,'Intel Core i7',1.80,'MBP22.88Z.00A2.B07.0708131123',5),(5,'Windows 7',8192,1,'Intel Core2 Duo E8500',3.16,'A01',6),(6,'Windows 7',8192,1,'Intel Core2 Duo E8500',3.16,'A01',7),(7,'Mac OS 10.7',4096,1,'Intel Core i7',2.53,'MBP22.88Z.00A4.B07.0708131428',10),(8,'Windows 7',4096,1,'Intel Core2 Duo E8500',3.16,'A01',11),(9,'Windows 7',4096,1,'Intel Core2 Duo E8500',3.16,'A01',13),(10,'Windows 7',4096,1,'Intel Core2 Duo T9600',2.80,'A02',15),(11,'Windows 7',4096,1,'Intel Core2 Duo T9600',2.80,'A02',16),(12,'Windows 7',4096,1,'Intel Core2 Duo E8500',3.16,'A01',19),(13,NULL,128,NULL,NULL,NULL,NULL,20),(14,'Windows 7',8192,1,'Intel Core2 Duo E8500',3.16,'A01',21),(15,'Windows 7',2048,1,'Intel Atom',1.60,'A05',22),(16,'Windows 7',8192,1,'Intel Core2 Duo SP9400',2.40,'A02',23),(17,'Mac OS 10.8',8192,1,'Intel Core i7',2.80,'MP41.0081.B07',24),(18,NULL,512,NULL,NULL,NULL,NULL,25),(19,'Windows 7',4096,1,'Intel Core i5 650',3.20,'A04',28),(20,'Windows 7',4096,1,'Intel Core i5 650',3.20,'A04',32),(21,NULL,128,NULL,NULL,NULL,NULL,33),(22,'Windows 7',4096,1,'Intel Core i5 M520',2.40,'A04',34),(23,'Windows 7',4096,1,'Intel Core i5 650',3.20,'A04',36),(24,NULL,512,NULL,NULL,NULL,NULL,37),(25,'Mac OS 10.8',8192,1,'Intel Core i7',2.53,'MP43.0081.B08',38),(26,'Windows Server 2008R2',16384,2,'Intel Xeon',3.20,'A06',39),(27,'Windows Server 2008R3',16384,1,'Intel Xeon',3.00,'A03',40),(28,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A02',41),(29,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A02',42),(30,'Windows 8',4096,1,'Intel Core i7-3520M',2.90,'3.03',45),(31,'Windows 8',4096,1,'Intel Core i7-3770',3.40,'A12',46),(32,'Windows 7',4096,1,'Intel Core i5-3317U',1.60,'A10',47),(33,'Windows 8',8192,1,'Intel Core i7-3520M',2.90,'A02',49),(34,'Mac OS 10.8',16384,1,'Intel Xeon W3565',3.32,'MP51.88Z.007F.B03.1010071432',50),(35,'Mac OS 10.8',32707,1,'Intel Core i7-3770',3.40,'IM131.88Z.010A.B04.1210121459',51),(36,'Windows 8',16384,1,'Intel Core i7-3520M',2.90,'2.01',52),(37,'Windows 8',8192,1,'Intel Core i5-3470',3.20,'A12',54),(38,'Mac OS 10.8',8192,1,'Intel Core i7',2.53,'MP43.0081.B08',55),(39,'Windows 8',8192,1,'Intel Core i5-3470',3.20,'A12',57),(40,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',58),(41,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',59),(42,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',60),(43,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',61),(44,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',62),(45,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',63),(46,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',64),(47,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',65),(48,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',66),(49,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',67),(50,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',68),(51,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',69),(52,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',70),(53,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',71),(54,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',72),(55,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',73),(56,'Windows 8',8192,1,'Intel Core i5-2520M',2.50,'A01',74);
/*!40000 ALTER TABLE `composant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `dirigeant`
--

DROP TABLE IF EXISTS `dirigeant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `dirigeant` (
  `NoEmploye` int(11) NOT NULL,
  `NoEmplSuperieur` int(11) NOT NULL,
  PRIMARY KEY (`NoEmploye`,`NoEmplSuperieur`),
  KEY `NoEmpDirigeant_idx` (`NoEmploye`),
  KEY `NoEmplSupDirigeant_idx` (`NoEmplSuperieur`),
  CONSTRAINT `NoEmplSupDirigeant` FOREIGN KEY (`NoEmplSuperieur`) REFERENCES `utilisateur` (`NoEmploye`) ON UPDATE CASCADE,
  CONSTRAINT `NoEmpDirigeant` FOREIGN KEY (`NoEmploye`) REFERENCES `utilisateur` (`NoEmploye`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `dirigeant`
--

LOCK TABLES `dirigeant` WRITE;
/*!40000 ALTER TABLE `dirigeant` DISABLE KEYS */;
INSERT INTO `dirigeant` VALUES (1,27),(2,12),(3,12),(4,26),(5,31),(6,31),(7,31),(8,4),(9,4),(10,26),(11,27),(12,26),(13,11),(14,24),(15,24),(17,11),(18,12),(19,27),(20,24),(21,24),(22,12),(23,26),(24,26),(25,27),(26,26),(27,26),(28,26),(29,11),(30,26),(31,26),(32,31),(33,31),(34,31),(35,31),(36,31),(37,12),(38,24),(39,10),(40,10);
/*!40000 ALTER TABLE `dirigeant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `equipement`
--

DROP TABLE IF EXISTS `equipement`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `equipement` (
  `NoInventaire` int(11) NOT NULL AUTO_INCREMENT,
  `NoSerie` varchar(255) NOT NULL,
  `Designation` varchar(255) NOT NULL,
  `DReception` date NOT NULL,
  `DFinGarantie` date NOT NULL,
  `NoCommande` varchar(255) NOT NULL,
  `Prix` float(7,2) NOT NULL,
  `DInventaire` date DEFAULT NULL,
  `TxtGarantie` varchar(255) DEFAULT NULL,
  `Remarque` varchar(255) DEFAULT NULL,
  `Taille` float(3,1) DEFAULT NULL,
  `Nom` varchar(255) DEFAULT NULL,
  `FormatMax` int(11) DEFAULT NULL,
  `RectoVerso` tinyint(1) DEFAULT '0',
  `Couleur` tinyint(1) DEFAULT '0',
  `ModifEnCours` tinyint(1) DEFAULT '0',
  `NoEmploye` int(11) DEFAULT NULL,
  `IDFournisseur` int(11) NOT NULL,
  `IDClasse` int(11) NOT NULL,
  PRIMARY KEY (`NoInventaire`),
  UNIQUE KEY `NoInventaire_UNIQUE` (`NoInventaire`),
  KEY `NoEmpEquipement_idx` (`NoEmploye`),
  KEY `IDFournEquipement_idx` (`IDFournisseur`),
  KEY `IDClsEquipement_idx` (`IDClasse`),
  KEY `IDFormatEquipement_idx` (`FormatMax`),
  CONSTRAINT `IDFormatEquipement` FOREIGN KEY (`FormatMax`) REFERENCES `formatimpression` (`IDFormat`) ON UPDATE CASCADE,
  CONSTRAINT `IDClsEquipement` FOREIGN KEY (`IDClasse`) REFERENCES `classetechnique` (`IDClasse`) ON UPDATE CASCADE,
  CONSTRAINT `IDFournEquipement` FOREIGN KEY (`IDFournisseur`) REFERENCES `fournisseur` (`IDFournisseur`) ON UPDATE CASCADE,
  CONSTRAINT `NoEmpEquipement` FOREIGN KEY (`NoEmploye`) REFERENCES `utilisateur` (`NoEmploye`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=75 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `equipement`
--

LOCK TABLES `equipement` WRITE;
/*!40000 ALTER TABLE `equipement` DISABLE KEYS */;
INSERT INTO `equipement` VALUES (1,'HN1ZH4J','Latitude E6400','2009-10-28','2012-10-28','30001426',1825.75,'2012-10-23','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC12',NULL,0,0,0,29,1,1),(2,'CNCKB15112','Laserjet P2055 DN','2009-08-28','2010-06-28','30001427',5257.00,'2012-10-23','1 année',NULL,NULL,'GTKLP1',3,1,0,0,16,2,5),(3,'CZ-0F525M-73608-06M-2UEL','U2410 Ultrasharp','2010-07-13','2013-07-13','30001428',254.25,'2012-10-23','3Yr Premium Panel Exchange Service',NULL,24.0,NULL,NULL,0,0,0,2,1,4),(4,'W89254EN64C','MacBook Pro 15-inch','2009-06-25','2012-06-25','30001429',2502.80,'2012-10-24','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC19',NULL,0,0,0,1,4,1),(5,'65HPC4J','Macbook Air','2009-06-29','2012-06-29','30001430',2689.15,'2012-10-24','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC13',NULL,0,0,0,4,4,1),(6,'DNTPC4J','OptiPlex 960 MT','2009-06-29','2012-06-29','30001431',1007.45,'2012-10-24','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC14',NULL,0,0,0,2,1,2),(7,'W89250EY9A8','OptiPlex 960 MT','2009-06-29','2012-06-29','30001432',1007.45,'2012-10-24','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC15',NULL,0,0,0,3,1,2),(8,'5QPQC4J','22in P2210 wide screen','2009-06-29','2012-06-29','30001433',249.80,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,3,1,4),(9,'4QPQC4J','22in P2210 wide screen','2009-06-29','2012-06-29','30001434',249.80,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,5,1,4),(10,'W89263G366J','MacBook Pro 13\"','2009-07-02','2012-07-02','30001435',2366.00,'2012-10-25','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC16',NULL,0,0,0,28,4,1),(11,'69ZYC4J','Optiplex 960 SF','2009-07-03','2013-07-03','30001436',987.55,'2012-10-25','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC17',NULL,0,0,0,5,1,2),(12,'CN0W631F7287295D1RHS','22in E2209W Swiss Black Widescreen','2009-07-03','2012-07-03','30001437',249.80,'2012-10-25','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,6,1,4),(13,'JP3ZC4J','OptiPlex 960 SF','2009-07-03','2013-07-03','30001438',987.55,'2012-10-24','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC1',NULL,0,0,0,6,1,2),(14,'5ZMSTH1','27in 2709W Swiss Black Widescreen UltraSharp (1920x1200)','2009-07-03','2012-07-03','30001439',436.00,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,8,1,4),(15,'85614G1','Latitude E6500','2009-10-06','2012-10-06','30001440',1789.90,'2012-10-24','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC1',NULL,0,0,0,NULL,1,1),(16,'DGX0D4J','Latitude E6500','2009-10-06','2012-10-06','30001441',1789.90,'2012-10-24','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC2',NULL,0,0,0,NULL,1,1),(17,'CZ0G286H7426396424JS',' 2209WA Euro Black Widescreen UltraSharp (1680x1050)','2009-10-06','2012-10-06','30001442',249.80,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,9,1,4),(18,'7HC4D4J','Ecran 22\" P2210\"','2010-01-12','2013-01-12','30001443',249.80,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,7,1,4),(19,'468WL4J','OptiPlex 960 SF','2010-01-13','2014-01-13','30001444',987.55,'2012-10-24','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC3',NULL,0,0,0,7,1,2),(20,'5Z8YL4J','HP Laserjet P2055 DN','2010-01-15','2013-01-15','30001445',5186.35,'2012-10-29','HP CarePack 4Y/NBD',NULL,NULL,'GTKLP2',3,1,0,0,16,2,5),(21,'2A91068W0K0','OptiPlex 960 SF','2010-02-17','2015-02-17','30001446',980.85,'2012-10-25','5Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC3',NULL,0,0,0,12,1,2),(22,'CK93200C20H','Latitude L2100','2010-02-25','2013-02-25','30001447',1324.00,'2012-10-25','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC4',NULL,0,0,0,NULL,1,1),(23,'DK9HF4J','Latitude E4300 : BLACK Intel Core 2 Duo SP9400(2.40GHz,1066MHz,6MB)','2010-03-08','2013-03-08','30001448',1766.45,'2012-10-24','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC5',NULL,0,0,0,10,1,1),(24,'CKWQF4J','iMac 27-inch 2.8GHz Quad-Core Intel Core i7','2010-05-07','2013-05-07','30001449',3221.20,'2012-10-24','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC7',NULL,0,0,0,11,4,2),(25,'DKWQF4J','HP Color LaserJet CP4525DN','2010-12-31','2014-01-31','30001450',6555.00,'2012-10-25','HP CarePack 4Y/NBD',NULL,NULL,'GTKLP3',3,1,1,0,16,4,5),(26,'CN0W631F7287295D1RHX','22in E2209W Swiss Black Widescreen','2011-07-03','2014-07-03','30001451',235.00,'2012-10-25','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,12,1,4),(27,'41688K1','Ecran 27\" Apple Cinema','2011-01-27','2014-01-21','30001452',1002.25,'2012-10-23','AppleCare Protection Plan 3Y',NULL,27.0,NULL,NULL,0,0,0,23,1,4),(28,'VM937G7D259','Optiplex 980SF','2011-02-04','2015-02-04','30001453',995.65,'2012-10-25','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC6',NULL,0,0,0,13,1,2),(29,'WM937G2S0TH','Ecran noir 22\" P2210','2011-02-04','2014-02-04','30001454',242.30,'2012-10-25','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,13,1,4),(30,'W89382TB66J',' 2209WA Euro Black Widescreen UltraSharp (1680x1050)','2009-10-06','2012-10-06','30001455',242.30,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,14,1,4),(31,'FXQP8K1','22in P2210 Swiss Black Widescreen Professional (1680 x 1050)','2011-02-16','2014-02-16','30001456',242.30,'2012-10-23','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,15,1,4),(32,'C6P1H4J','OptiPlex 980 SF','2011-02-16','2016-02-16','30001457',995.65,'2012-10-23','5Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC8',NULL,0,0,0,14,1,2),(33,'G6P1H4J','HP LaserJet P2055dn','2011-02-16','2014-02-16','30001458',5123.55,'2012-10-23','HP CarePack 4Y/NBD',NULL,NULL,'GTKLP3',3,1,0,0,16,2,5),(34,'D6P1H4J','Latitude E6410','2011-02-18','2015-02-18','30001459',1789.60,'2012-10-29','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC9',NULL,0,0,0,16,1,1),(35,'8QV1H4J','Moniteur 22\" P2210','2011-03-03','2014-03-03','30001459',238.10,'2012-10-24','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,16,1,4),(36,'9QV1H4J','Optiplex 980SF','2011-03-03','2016-03-03','30001460',989.75,'2012-10-24','5Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC10',NULL,0,0,0,15,1,2),(37,'F6P1H4J','HP Color LaserJet CP4525DN','2011-04-08','2014-04-08','30001461',6423.00,'2012-10-29','HP CarePack 4Y/NBD',NULL,NULL,'GTKLP4',3,1,1,0,16,2,5),(38,'CN0H735H7287298643RL','MacBook Pro 15-inch 2.0GHz Intel Core i7','2011-04-12','2014-04-12','30001462',1755.85,'2012-10-25','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC11',NULL,0,0,0,23,4,1),(39,'CN-0H735H-72872-986-4MLL','PowerEdge R310','2011-05-13','2014-05-13','30001463',7123.95,'2012-10-25','3Yr ProSupport and NBD On-Site Service',NULL,NULL,'GTKSRV1',NULL,0,0,0,16,1,3),(40,'6SKWTJ1','PowerEdge R610','2011-05-18','2016-05-18','30001463',8512.25,'2012-10-25','3Yr ProSupport and NBD On-Site Service',NULL,NULL,'GTKSRV2',NULL,0,0,0,16,1,3),(41,'CZ0G286H742639A81W7S','Dell Latitude E6320','2011-07-05','2015-07-05','30001464',1545.90,'2012-10-29','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC18',NULL,0,0,0,19,1,1),(42,'1DTQH4J','Latitude E6320','2011-07-05','2014-07-05','30001464',1545.90,'2012-10-25','3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC20',NULL,0,0,0,17,1,1),(43,'7GX6J4J','Ecran Dell P2211H','2011-07-14','2014-07-14','30001464',225.00,'2012-10-25','3Yr Premium Panel Exchange Service',NULL,22.0,NULL,NULL,0,0,0,17,1,4),(44,'W89443J75PK','Dell UltraSharp U2711','2012-12-07','2015-12-07','30001464',389.60,'2012-10-25','3Yr Premium Panel Exchange Service',NULL,27.0,NULL,NULL,0,0,0,18,1,4),(45,'W89443J85PK','Lenovo T430S','2012-12-07','2015-12-07','30001465',2820.00,'2012-10-25','3 ans NBD',NULL,NULL,'GTK-LSPC21',NULL,0,0,0,18,3,1),(46,'W89443J65PK','Dell OptiPlex 9010 SF','2012-11-30','2017-11-30','30001466',966.00,'2012-10-25','5Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC22',NULL,0,0,0,20,1,2),(47,'BN88J4J','Dell XPS 13','2012-12-21','2015-12-21','30001467',1200.20,'2012-10-25','3Yr Basic Warranty - Next Business Day',NULL,NULL,'CDM-SFI-LLPC13',NULL,0,0,0,20,1,1),(48,'CHY4K4J','Moniteur Apple LED Cinema Display (Ecran plat de 27 pouces)','2013-01-07','2016-01-07','30001468',1489.60,'2012-10-25','AppleCare Protection Plan 3Y',NULL,27.0,NULL,NULL,0,0,0,26,4,4),(49,'BHY4K4J','Dell Latitude E6430','2013-01-07','2017-01-07','30001469',1689.55,'2012-10-25','4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC23',NULL,0,0,0,NULL,1,1),(50,'9HY4K4J','Mac Pro 3.2GHz Quad-Core Intel Xeon','2013-01-08','2016-01-08','30001470',3354.00,'2012-10-25','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC24',NULL,0,0,0,26,4,2),(51,'HAV130261','iMac 27\"','2013-01-10','2016-01-10','30001471',3524.10,'2012-10-25','AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC25',NULL,0,0,0,24,4,2),(52,'DMGHL4J','Lenovo X230T','2013-01-10','2016-01-10','30001472',2054.00,'2012-10-25','3 ans NBD',NULL,NULL,'GTK-LSPC26',NULL,0,0,0,27,3,1),(53,'CN091D1V7444599F802L','Dell UltraSharp U2412M','2013-03-06','2016-03-06','30001473',375.20,NULL,'3Yr Premium Panel Exchange Service',NULL,24.0,NULL,NULL,0,0,0,21,1,4),(54,'CMGHL4J','OptiPlex 9010 SF','2013-03-06','2017-03-13','30001473',988.50,NULL,'4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC27',NULL,0,0,0,21,1,2),(55,'CN091D1V7444599F878L','Macbook Pro 15\" Retina Display','2013-03-13','2016-03-13','30001474',1745.20,NULL,'AppleCare Protection Plan 3Y',NULL,NULL,'GTK-LSPC28',NULL,0,0,0,NULL,4,1),(56,'D6PGJ4J','Ecran Dell UltraSharp U2412M 61 cm (24\'\')','2013-03-13','2016-03-13','30001475',357.25,NULL,'3Yr Premium Panel Exchange Service',NULL,24.0,NULL,NULL,0,0,0,38,1,4),(57,'7RT1K4J','Dell Optiplex 9010SF','2013-05-23','2017-05-23','30001475',988.55,NULL,'4Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC29',NULL,0,0,0,38,1,2),(58,'8WVNJ4J','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC30',NULL,0,0,0,30,1,1),(59,'8WVNJ4K','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC30',NULL,0,0,0,31,1,1),(60,'8WVNJ4L','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC31',NULL,0,0,0,32,1,1),(61,'8WVNJ4M','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC32',NULL,0,0,0,33,1,1),(62,'8WVNJ4N','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC33',NULL,0,0,0,34,1,1),(63,'8WVNJ4O','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC34',NULL,0,0,0,35,1,1),(64,'8WVNJ4P','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC35',NULL,0,0,0,36,1,1),(65,'8WVNJ4Q','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC36',NULL,0,0,0,37,1,1),(66,'8WVNJ4R','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC37',NULL,0,0,0,38,1,1),(67,'8WVNJ4S','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC38',NULL,0,0,0,39,1,1),(68,'8WVNJ4T','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC39',NULL,0,0,0,40,1,1),(69,'8WVNJ4U','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC40',NULL,0,0,0,25,1,1),(70,'6ZVNJ4V','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC41',NULL,0,0,0,22,1,1),(71,'2SVNJ4W','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC42',NULL,0,0,0,8,1,1),(72,'0AVNJ4X','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC43',NULL,0,0,0,9,1,1),(73,'7HVNJ4Y','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC44',NULL,0,0,0,NULL,1,1),(74,'5NVNJ4Z','Latitude E6230','2013-05-28','2016-05-28','30001476',1570.00,NULL,'3Yr Basic Warranty - Next Business Day',NULL,NULL,'GTK-LSPC45',NULL,0,0,0,NULL,1,1);
/*!40000 ALTER TABLE `equipement` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `formatimpression`
--

DROP TABLE IF EXISTS `formatimpression`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `formatimpression` (
  `IDFormat` int(11) NOT NULL AUTO_INCREMENT,
  `Libelle` varchar(255) NOT NULL,
  PRIMARY KEY (`IDFormat`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `formatimpression`
--

LOCK TABLES `formatimpression` WRITE;
/*!40000 ALTER TABLE `formatimpression` DISABLE KEYS */;
INSERT INTO `formatimpression` VALUES (1,'A0'),(2,'A3'),(3,'A4'),(4,'Letter');
/*!40000 ALTER TABLE `formatimpression` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fournisseur` (
  `IDFournisseur` int(11) NOT NULL AUTO_INCREMENT,
  `RaisonSociale` varchar(255) NOT NULL,
  `Rue` varchar(255) DEFAULT NULL,
  `NoRue` int(11) DEFAULT NULL,
  `NPA` int(11) NOT NULL,
  `Ville` varchar(255) NOT NULL,
  `Pays` varchar(255) NOT NULL,
  `Tel` varchar(255) DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`IDFournisseur`),
  UNIQUE KEY `IDFournisseur_UNIQUE` (`IDFournisseur`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fournisseur`
--

LOCK TABLES `fournisseur` WRITE;
/*!40000 ALTER TABLE `fournisseur` DISABLE KEYS */;
INSERT INTO `fournisseur` VALUES (1,'Dell SA','Route de l\'Aéroport',29,1215,'Genève','Suisse','0848 33 55 01','ch_customercare_geneva@dell.com'),(2,'Hewlett-Packard','Route du Nant-d\'Avril',150,1217,'Meyrin','Suisse','022 780 81 11','customercare_meyrin@hp.com'),(3,'Lenovo GmbH','Zürcherstrasse',39,8952,'Schlieren ','Suisse','044 755 56 57','info_ch@lenovo.com'),(4,'Apple Inc','Infinite Loop',NULL,95014,'Cupertino','USA','0805 540 303',NULL);
/*!40000 ALTER TABLE `fournisseur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `localisation`
--

DROP TABLE IF EXISTS `localisation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `localisation` (
  `CodeLocal` int(11) NOT NULL AUTO_INCREMENT,
  `NoLocal` varchar(255) NOT NULL,
  PRIMARY KEY (`CodeLocal`),
  UNIQUE KEY `CodeLocal_UNIQUE` (`CodeLocal`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `localisation`
--

LOCK TABLES `localisation` WRITE;
/*!40000 ALTER TABLE `localisation` DISABLE KEYS */;
INSERT INTO `localisation` VALUES (1,'101'),(2,'102'),(3,'103'),(4,'104'),(5,'105'),(6,'106'),(7,'107'),(8,'108'),(9,'109'),(10,'110'),(11,'111'),(12,'112'),(13,'113'),(14,'114a'),(15,'114b');
/*!40000 ALTER TABLE `localisation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `utilisateur`
--

DROP TABLE IF EXISTS `utilisateur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `utilisateur` (
  `NoEmploye` int(11) NOT NULL AUTO_INCREMENT,
  `Nom` varchar(255) NOT NULL,
  `Prenom` varchar(255) NOT NULL,
  `Departement` varchar(255) NOT NULL,
  `Fonction` varchar(255) NOT NULL,
  `CodeLocal` int(11) DEFAULT NULL,
  PRIMARY KEY (`NoEmploye`),
  UNIQUE KEY `NoEmploye_UNIQUE` (`NoEmploye`),
  KEY `CodLocUtilisateur_idx` (`CodeLocal`),
  CONSTRAINT `CodLocUtilisateur` FOREIGN KEY (`CodeLocal`) REFERENCES `localisation` (`CodeLocal`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `utilisateur`
--

LOCK TABLES `utilisateur` WRITE;
/*!40000 ALTER TABLE `utilisateur` DISABLE KEYS */;
INSERT INTO `utilisateur` VALUES (1,'Bonardi','Martine','Marketing','Sociologue',2),(2,'Buser','Marianne','Vente','Vendeur',6),(3,'Cachin','Rosangela','Vente','Vendeur',6),(4,'Crausaz','Pierre','Externe','Consultant scientifique',7),(5,'Paris','Jeannine','Service client','Secrétaire',8),(6,'Suard','Martine','Service client','Secrétaire',8),(7,'Tiercy','Christiane','Service client','Secrétaire',8),(8,'Wahl','Maria','Archive','Bibliothécaire',10),(9,'Wdzieczkowska','Valérie','Archive','Bibliothécaire',10),(10,'Charbonnier','Alexandra','Relations externe','Responsable de la communication',4),(11,'Von Schack','Marthe','Marketing','Secrétaire',2),(12,'Lindsay','Patrick','Vente','Responsable des ventes',9),(13,'Léonard','Leonore','Informatique','Informaticienne',11),(14,'Golay','Kristian','R&D','Economiste',1),(15,'Sund','Fariba','R&D','Economiste',1),(16,'Hashemi','Pascal','Informatique','Responsable informatique',11),(17,'Bangerter','Emanuela','Informatique','Secrétaire',11),(18,'Mancianti','Sandrine','Vente','Secrétaire',12),(19,'Baxevanidis','Beat','Marketing','Economiste',2),(20,'Wyser','Valérie','R&D','Secrétaire',1),(21,'Schaerer','Diwata','R&D','Secrétaire',1),(22,'Hunziker','Emilie','Vente','Vendeur',6),(23,'Procnard','Suzanne','Direction','Secrétaire',14),(24,'Buffat','Agata','R&D','Responsable R&D',5),(25,'Duca','Ismail','Marketing','Politologue',2),(26,'Alyanak','Barbara','Direction','Directeur',15),(27,'Baumann','Mathieu','Marketing','Responsable marketing',2),(28,'d\'Acremont','Lorenz','Externe','Consultant scientifique',7),(29,'Lehmann','Grégoire','Informatique','Informaticien',11),(30,'Bovy','Stephan','Externe','Consultant',7),(31,'Bieri','Ernesto','Service client','Responsable service client',13),(32,'Glenn','Chiara','Service client','Economiste',13),(33,'Forlati','Viviana','Service client','Economiste',13),(34,'Munoz','Carina','Service client','Economiste',13),(35,'Lomberg','Claudia','Service client','Economiste',13),(36,'Ravanelli','Erik','Service client','Economiste',13),(37,'Swars','Alwin','Vente','Coordinateur',12),(38,'Kopse','Roxane','R&D','Coordinatrice',1),(39,'Stanford','Valérie','Relations externe','Secrétaire',3),(40,'Maillard','Carole','Relations externe','Assistant communication',3);
/*!40000 ALTER TABLE `utilisateur` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2013-06-22 23:31:57
