package helpers;

import java.awt.Color;
import javax.swing.BorderFactory;
import javax.swing.JTextArea;

@SuppressWarnings("serial")
public class TextAreaHelper extends JTextArea{

	public TextAreaHelper(String text){
		super(text);
	}
	
	/*
	 * Change la couleur par d�faut du setEnabled
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean isEnabled){
		super.setEnabled(isEnabled);
		if(isEnabled == false){
			this.setDisabledTextColor(new Color(106,153,200));
			this.setBorder(BorderFactory.createLineBorder(new Color(106,153,200)));
		}
	}

}
