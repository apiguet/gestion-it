package helpers;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.SupplierModel;

@SuppressWarnings("serial")
public class SupplierTableExtend extends AbstractTableModel {
	private String[] columnNames;
	ArrayList<SupplierModel> list = null;

	public SupplierTableExtend(ArrayList<SupplierModel> list, String[] header) {
		this.columnNames = header;
		this.list = list;
	}

	public int getColumnCount() {
		return this.columnNames.length;
	}

	public int getRowCount() {
		return this.list.size();
	}

	public String getColumnName(int col) {
		return this.columnNames[col];
	}
	
	public void setModel(ArrayList<SupplierModel> list){
		this.list = list;
	}

	public Object getValueAt(int row, int col) {

		SupplierModel object = this.list.get(row);

		switch (col) {
		case 0:
			return object.getId();
		case 1:
			return object.getRaisonSociale();
		case 2:
			return object.getRue();
		case 3:
			if (object.getNo() == 0) {
				return "";
			} else {
				return object.getNo();
			}
		case 4:
			return object.getNpa();
		case 5:
			return object.getVille();
		case 6:
			return object.getPays();		
		case 7:
			return object.getTel();
		case 8:
			return object.getEmail();
		default:
			return "unknown";
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return this.list.getClass();
	}
	
	public void addSupplier(SupplierModel sm) {
        // Adds the element in the last position in the list
        list.add(sm);
        fireTableRowsInserted(list.size()-1, list.size()-1);
    }
 
    public void removeSupplier(int rowIndex) {
        list.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    
    public void updateSupplier(int rowIndex, SupplierModel sm) {
        list.set(rowIndex, sm);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }

}
