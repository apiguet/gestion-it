package helpers;

import java.awt.Component;
import java.awt.FocusTraversalPolicy;
import java.util.Vector;
import com.sun.xml.internal.ws.api.server.Container;

public class focusTraversalPolicyHelper extends FocusTraversalPolicy{
	Vector<Component> order;
	
	public focusTraversalPolicyHelper(Vector<Component> order) {
		this.order = new Vector<Component>(order.size());
		this.order.addAll(order);
	}
	
	public Component getComponentAfter(Container focusCycleRoot,Component aComponent){
		int idx = (order.indexOf(aComponent) + 1) % order.size();
		return order.get(idx);
	}
	
	public Component getComponentBefore(Container focusCycleRoot, Component aComponent){
		int idx = order.indexOf(aComponent) - 1;
		if (idx < 0) {
			idx = order.size() - 1;
		}
		return order.get(idx);
	}
	
	public Component getDefaultComponent(Container focusCycleRoot) {
		return order.get(0);
	}
	
	public Component getLastComponent(Container focusCycleRoot) {
		return order.lastElement();
	}
	
	public Component getFirstComponent(Container focusCycleRoot) {
		return order.get(0);
	}

	@Override
	public Component getComponentAfter(java.awt.Container aContainer,
			Component aComponent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getComponentBefore(java.awt.Container aContainer,
			Component aComponent) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getDefaultComponent(java.awt.Container aContainer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getFirstComponent(java.awt.Container aContainer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Component getLastComponent(java.awt.Container aContainer) {
		// TODO Auto-generated method stub
		return null;
	}
}
