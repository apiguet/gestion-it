package helpers;

import java.util.ArrayList;
import javax.swing.table.AbstractTableModel;
import model.EquipementModel;

@SuppressWarnings("serial")
public class TableExtend extends AbstractTableModel {
	private String[] columnNames;
	ArrayList<EquipementModel> list = null;

	public TableExtend(ArrayList<EquipementModel> list, String[] header) {
		this.columnNames = header;
		this.list = list;
	}

	public int getColumnCount() {
		return this.columnNames.length;
	}

	public int getRowCount() {
		return this.list.size();
	}

	public String getColumnName(int col) {
		return this.columnNames[col];
	}
	
	public void setModel(ArrayList<EquipementModel> list){
		this.list = list;
	}

	/*
	 * Retourne la valeur selon la ligne et colone du tableau
	 * @see javax.swing.table.TableModel#getValueAt(int, int)
	 */
	public Object getValueAt(int row, int col) {

		EquipementModel object = this.list.get(row);

		switch (col) {
		case 0:
			return object.getNoInventaire();
		case 1:
			return object.getNomFournisseur();
		case 2:
			return object.getNumSerie();
		case 3:
			return object.getNom();
		case 4:
			return object.getDesignation();
		case 5:
			return object.getnomAttribueA();
		case 6:
			return object.getLocalisation();		
		case 7:
			return object.getMiseService();
		case 8:
			return object.getEcheance();
		default:
			return "unknown";
		}
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Class getColumnClass(int c) {
		return this.list.getClass();
	}
	
	public void addEquipement(EquipementModel em) {
        // Adds the element in the last position in the list
        list.add(em);
        fireTableRowsInserted(list.size()-1, list.size()-1);
    }
 
    public void removeEquipement(int rowIndex) {
        list.remove(rowIndex);
        fireTableRowsDeleted(rowIndex, rowIndex);
    }
    
    public void updateEquipement(int rowIndex, EquipementModel em) {
        list.set(rowIndex, em);
        fireTableRowsUpdated(rowIndex, rowIndex);
    }
}

