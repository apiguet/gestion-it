package helpers;

import java.awt.Color;
import java.text.DateFormat;
import javax.swing.BorderFactory;
import javax.swing.JFormattedTextField;

@SuppressWarnings("serial")
public class FormattedTextFieldHelper extends JFormattedTextField{

	public FormattedTextFieldHelper(DateFormat df){
		super(df);
	}
	
	/*
	 * Change la couleur par d�faut du setEnabled
	 * @see javax.swing.JComponent#setEnabled(boolean)
	 */
	@Override
	public void setEnabled(boolean isEnabled){
		super.setEnabled(isEnabled);
		if(isEnabled == false){
			this.setDisabledTextColor(new Color(106,153,200));
			this.setBorder(BorderFactory.createLineBorder(new Color(106,153,200)));
		}
	}


}
