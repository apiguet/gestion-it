package helpers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;
import model.SupplierModel;
import com.sun.rowset.CachedRowSetImpl;

public class Connect {
	private static final String DB_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_CONNECTION = "jdbc:mysql://localhost:3306/gestion_it?zeroDateTimeBehavior=convertToNull";
	private static final String DB_USER = "GestITAdmin";
	private static final String DB_PASSWORD = "tgvfgh";
	
	/*
	 * Fonction de connection � la base de donn�es
	 * @return: Connection
	 */
	private static Connection getDBConnection() { 
		Connection dbConnection = null;
		try {
			Class.forName(DB_DRIVER);
		} catch (ClassNotFoundException e) {
			System.out.println(e.getMessage());
		}
		
		try {
			dbConnection = DriverManager.getConnection(DB_CONNECTION, DB_USER,DB_PASSWORD);
			return dbConnection;
		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
		return dbConnection;
	}

	/*
	 * Effectue la requ�te de type SELECT pass�e en param�tre
	 * @param: String -> requ�te
	 * @result: CachedRowSet -> r�sultats de la requ�te
	 */
	public CachedRowSet queryDB (String query) {
		Connection dbConnection = null;
		Statement stmt = null;
		ResultSet rs = null;
		CachedRowSet rowset = null;
			
		try {
			dbConnection = getDBConnection();
			stmt = dbConnection.createStatement();
			rs = stmt.executeQuery(query);
			
			rowset = new CachedRowSetImpl();
			//propri�t�s du RowSet
			rowset.setType(ResultSet.TYPE_SCROLL_INSENSITIVE);
			rowset.setConcurrency(ResultSet.CONCUR_UPDATABLE);
			
			//peuplement du RowSet avec les donn�es du ResultSet
			rowset.populate(rs);	
			
		} catch (Exception e) {
			System.out.println("Eurreur: connexion � la base impossible");
			//e.printStackTrace();
		} finally {
			try { if (rs != null) rs.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (dbConnection != null) dbConnection.close(); } catch (SQLException e) { e.printStackTrace(); }
		}
		
		return rowset;
	}	
	
	/*
	 * Effectue la requ�te de type INSERT, UPDATE ou DELETE pass�e en param�tre
	 * @param: String -> requ�te
	 * @return: int -> resultat de la requ�te
	 */
	public int updateDB (String query) {
		Connection dbConnection = null;
		Statement stmt = null;
		int rs = 0;
		
		try {
			dbConnection = getDBConnection();
			stmt = dbConnection.createStatement();
			rs = stmt.executeUpdate(query);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try { if (stmt != null) stmt.close(); } catch (SQLException e) { e.printStackTrace(); }
			try { if (dbConnection != null) dbConnection.close(); } catch (SQLException e) { e.printStackTrace(); }
		}	
		return rs;
	}
	
	/*
	 * Modifie ou ajoute un �quipement
	 * @param: HashMap -> l'�quipement, int -> id de l'�quipement
	 * @return: int -> resultat de la requ�te
	 */
	public int executeEquipementDB (HashMap<String, String> e, int id) {
		Connection dbConnection = null;
		int result = 0;
		
		try {
			dbConnection = getDBConnection();
			
			String update = "UPDATE equipement SET NoSerie = ?, Designation = ?, DReception = ?, DFinGarantie = ?, NoCommande = ?" +
					", Prix = ?, DInventaire = ?, TxtGarantie = ?, Remarque = ?, Taille = ?, Nom = ?" +
					", FormatMax = ?, RectoVerso = ?, Couleur = ?, ModifEnCours = ?, NoEmploye = ?, IDFournisseur = ?" +
					", IDClasse = ? WHERE NoInventaire = ?";
			
			String insert = "INSERT INTO equipement (NoSerie, Designation, DReception, DFinGarantie, NoCommande, " +
				"Prix, DInventaire, TxtGarantie, Remarque, Taille, Nom, FormatMax, RectoVerso, Couleur, ModifEnCours, " +
				"NoEmploye, IDFournisseur, IDClasse) " +
				"VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
			
			PreparedStatement preparedStatement;
			if (id == 0) {
				preparedStatement = dbConnection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);
			} else {
				preparedStatement = dbConnection.prepareStatement(update);
				preparedStatement.setInt(19, id);
			}
			preparedStatement.setString(1, e.get("jtfSerialNum"));
			preparedStatement.setString(2, e.get("jtfDesignation"));
			preparedStatement.setObject(3, e.get("jtfReception"), Types.DATE);
			preparedStatement.setObject(4, e.get("jtfAssu"), Types.DATE);
			preparedStatement.setString(5, e.get("jtfOrder"));
			preparedStatement.setFloat(6, Float.parseFloat(e.get("jtfCost")));
			if (e.get("jtfPhysInv").equals("NULL")) {
				preparedStatement.setObject(7, null);
			} else {
				preparedStatement.setObject(7, e.get("jtfPhysInv"), Types.DATE);
			}
			preparedStatement.setString(8, e.get("txtAssurance"));
			preparedStatement.setString(9, e.get("txtNote"));
			if (e.get("jtfSize").equals("NULL")) {
				preparedStatement.setObject(10, null);
			} else {
				preparedStatement.setObject(10, e.get("jtfSize"), Types.FLOAT);
			}
			preparedStatement.setString(11, e.get("jtfEqu"));
			if (e.get("listMaxSize").equals("NULL")) {
				preparedStatement.setObject(12, null);
			} else {
				preparedStatement.setObject(12, e.get("listMaxSize"), Types.INTEGER);
			}
			preparedStatement.setByte(13, Byte.parseByte(e.get("cbxDuplex")));
			preparedStatement.setByte(14, Byte.parseByte(e.get("cbxColor")));
			preparedStatement.setByte(15, Byte.parseByte(e.get("modifEnCours")));
			if (e.get("listAssigned").equals("NULL")) {
				preparedStatement.setObject(16, null);
			} else {
				preparedStatement.setObject(16, e.get("listAssigned"), Types.INTEGER);
			}
			preparedStatement.setInt(17, Integer.parseInt(e.get("listSupplier")));
			preparedStatement.setInt(18, Integer.parseInt(e.get("listTechClass")));
			
			result = preparedStatement.executeUpdate();
			
			if (id == 0 && result != 0){
				 ResultSet rs = preparedStatement.getGeneratedKeys();
				 rs.next();
				 result = rs.getInt(1);
			}
			
		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try { 
				if (dbConnection != null) dbConnection.close(); 
			} catch (SQLException ex) { 
				ex.printStackTrace(); 
			}
		}	
		return result;
	}
	
	/*
	 * Supprime un �quipement
	 * @param: int -> id de l'�quipement
	 * @return: int -> resultat de la requ�te
	 */
	public int deleteEquipementDB(int id) { 
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		int result;
 
		String delete = "DELETE FROM equipement WHERE noInventaire = ?";
		
		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(delete);
			preparedStatement.setInt(1, id);
 
			// execute delete SQL statement
			result = preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			result = e.getErrorCode();
		} finally {
			try { 
				if (dbConnection != null) dbConnection.close(); 
			} catch (SQLException ex) { 
				ex.printStackTrace(); 
			}
		}
		return result;
	}
	
	
	/*
	 * Modifie ou ajoute un fournisseur
	 * @param: SupplierModel -> le fournisseur
	 * @return: int -> resultat de la requ�te
	 */
	public int executeSupplierDB (SupplierModel sm) {
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		int result;
		String update = "UPDATE fournisseur SET RaisonSociale = ?, Rue = ?, NoRue = ?, NPA = ?, Ville = ?" +
				", Pays = ?, Tel = ?, Email = ? WHERE IDFournisseur = ?";
		
		String insert = "INSERT INTO fournisseur (RaisonSociale, Rue, NoRue, NPA, Ville, Pays, Tel, Email) " +
			"VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
		
		try {
			dbConnection = getDBConnection();
			
			if (sm.getId() == 0) {
				preparedStatement = dbConnection.prepareStatement(insert, Statement.RETURN_GENERATED_KEYS);	
			} else {
				preparedStatement = dbConnection.prepareStatement(update);
				preparedStatement.setInt(9, sm.getId());
			}
			preparedStatement.setString(1, sm.getRaisonSociale());
			preparedStatement.setString(2, sm.getRue());
			if (sm.getNo() == 0) {
				preparedStatement.setObject(3, null);
			} else {
				preparedStatement.setInt(3, sm.getNo());
			}
			preparedStatement.setInt(4, sm.getNpa());
			preparedStatement.setString(5, sm.getVille());
			preparedStatement.setString(6, sm.getPays());
			preparedStatement.setString(7, sm.getTel());
			preparedStatement.setString(8, sm.getEmail());
 
			// execute delete SQL statement
			result = preparedStatement.executeUpdate();
			
			//on r�cup�re l'id de l'insert
			if (sm.getId() == 0 && result != 0){
				 ResultSet rs = preparedStatement.getGeneratedKeys();
				 rs.next();
				 result = rs.getInt(1);
			}
			
		} catch (SQLException e) {
			System.out.println("erreurCOde: "+e.getMessage());
			result = e.getErrorCode();
		} finally {
			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
			if (dbConnection != null) {
				try {
					dbConnection.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return result;
	}
	
	/*
	 * Supprime un fournisseur
	 * @param: int -> id du fournisseur
	 * @return: int -> resultat de la requ�te
	 */
	public int deleteSupplierDB(int id) throws SQLException { 
		Connection dbConnection = null;
		PreparedStatement preparedStatement = null;
		int result;
 
		String delete = "DELETE FROM fournisseur WHERE IDFournisseur = ?";
		
		try {
			dbConnection = getDBConnection();
			preparedStatement = dbConnection.prepareStatement(delete);
			preparedStatement.setInt(1, id);
 
			// execute delete SQL statement
			result = preparedStatement.executeUpdate();
			
		} catch (SQLException e) {
			result = e.getErrorCode();
		} finally {
			if (preparedStatement != null) {
				preparedStatement.close();
			}
			if (dbConnection != null) {
				dbConnection.close();
			}
		}
		return result;
	}
	
}
