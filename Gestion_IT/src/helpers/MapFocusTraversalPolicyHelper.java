package helpers;

	import java.awt.Component;
	import java.awt.Container;
	import java.awt.FocusTraversalPolicy;
	import java.util.HashMap;
	 
	/**
	 * Permet de g�rer l'ordre de focus 
	 */
	public class MapFocusTraversalPolicyHelper extends FocusTraversalPolicy {
		private HashMap<Integer, Component> components = new HashMap<Integer, Component>();
		private HashMap<Component, Integer> positions = new HashMap<Component, Integer>();
		private int i = 0;
	 
		/**
		 * Ajoute le composant dans la liste ordr�e pour le focus
		 * 
		 * @param component Le composant
		 * @param boolean le champs est-il activ�?
		 */
		public void addComponent(Component component, boolean isEnabled){
			if(isEnabled == true){
				components.put(this.i, component);
				positions.put(component, this.i);
				this.i++;
			}
		}
	 
		@Override
		public Component getComponentAfter(Container parent, Component component) {
			int position = positions.get(component);
	 
			return components.get(position + 1);
		}
	 
		@Override
		public Component getComponentBefore(Container parent, Component component) {
			int position = positions.get(component);
	 
			return components.get(position - 1);
		}
	 
		@Override
		public Component getDefaultComponent(Container parent) {
			return components.get(0);
		}
	 
		@Override
		public Component getFirstComponent(Container parent) {
			return components.get(0);
		}
	 
		@Override
		public Component getLastComponent(Container parent) {
			return components.get(components.size() + 1);
		}
	}
