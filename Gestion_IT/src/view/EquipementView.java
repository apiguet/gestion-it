package view;

import helpers.FormattedTextFieldHelper;
import helpers.MapFocusTraversalPolicyHelper;
import helpers.TextAreaHelper;
import helpers.TextFieldHelper;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import controller.EquipementController;
import model.EquipementModel;

@SuppressWarnings("serial")
public class EquipementView extends JDialog {
	
	private EquipementModel model = new EquipementModel();
	private EquipementController equipementController = new EquipementController(this.model, this); 
	private JPanel container = new JPanel();
	private JPanel techDetails = new JPanel();
    private JTabbedPane tab = new JTabbedPane();
	private String statusForm = null;
	private int id;	
	private HashMap<String, String> fieldValues = new HashMap<String, String>();
	
	private String[] arrayTechClass = model.getTechClasses();
	private String[] arraySupplier = model.getSuppliers();
	private String[] arrayMaxSize = model.getMaxSize();
	private String[] arrayAssigned = model.getAssigned();
	
	//Boutons
	private JButton btEdit = new JButton(new ImageIcon(getClass().getResource("/view/edit.jpg")));
	private JButton btView = new JButton(new ImageIcon(getClass().getResource("/view/view.jpg")));
	private JButton btSave = new JButton("Enregistrer");
	
	//Champs mat�riel
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox listTechClass = new JComboBox(arrayTechClass);
	private JTextField jtfInv = new TextFieldHelper("");
	private JTextField jtfEqu;
	private JTextField jtfSerialNum = new TextFieldHelper("");
	private JTextField jtfAssu;
	private JTextField jtfDesignation = new TextFieldHelper("");
	private JTextField jtfPhysInv;
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox listAssigned = new JComboBox(arrayAssigned);
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox listSupplier = new JComboBox(arraySupplier);
	private JTextField jtfLocalisation = new TextFieldHelper("");
	private JTextField jtfOrder = new TextFieldHelper("");
	private JTextField jtfSup = new TextFieldHelper("");
	private JTextField jtfCost = new TextFieldHelper("");
	private JTextField jtfNameEqu = new TextFieldHelper("");
	private JTextField jtfSize = new TextFieldHelper("");
	private JTextField jtfNamePrint = new TextFieldHelper("");
	private JCheckBox cbxDuplex = new JCheckBox("Recto/verso");
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private JComboBox listMaxSize = new JComboBox(arrayMaxSize);
	private JCheckBox cbxColor = new JCheckBox("Couleur");
	
	private JTextArea txtAeraNote = new TextAreaHelper("Remarques concernant le mat�riel...");
	private JTextArea txtAeraAssurance = new TextAreaHelper("Texte concernant la garantie...");
	private String[] entetes = {"Libell�", "Valeur"}; 
	private Object[][] donnees = {};
    private JTable jTableComposant = new JTable(null, this.entetes);
	
	//label
	private JLabel labelNameEqu = new JLabel("Nom de la machine");
    private JLabel labelNamePrint = new JLabel("Nom de la machine");
    private JLabel labelMaxSize = new JLabel("Format maximum");
    private JLabel labelSize = new JLabel("Taille");
    
	
    //constructeur
	public EquipementView(MainView frame, EquipementModel model, String type, int id){
		super(frame, true);
	    this.model = model;
	    
	    //change couleur comboBox d�sactiv�e
	    UIManager.put("ComboBox.disabledForeground", new Color(106,153,200));
	    
	    //D�finition des propri�t�s de la fen�tre:
	    //Titre
	    this.setTitle("Gestion IT - Fiche mat�riel");
	    //Taille
	    this.setSize(800, 600);
	    //Fermeture de la fen�tre par la croix
	    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    //Objet centrer
	    this.setLocationRelativeTo(null);
	    //Emp�che le redimensionnement de la fen�tre
	    this.setResizable(false);
	    //Laisse la fen�tre toujours au premier plan
	    this.setAlwaysOnTop(false);
	    
	    this.id = id;
	    //r�cup�re les valeurs des champs suivant l'id
	    if (this.id != 0) getFields();
	    
	    //Change format des champs pour les dates
	    this.formatField();
	    
	    this.buildUI();
	    this.equipementDisplay(type);
	    
	    //affiche les valeurs des champs suivant l'id
	    if (this.id != 0) displayFields();
	   
	    //le panel sera le content pane
	    this.setContentPane(this.container);  
	    
	    this.addWindowListener(this.equipementController);
	   
	    //fen�tre visible       
	    this.setVisible(true);
	}
	
	/*
	 * G�n�re le format des champs
	 */
	private void formatField(){
	    DateFormat df = new SimpleDateFormat("yyyy-mm-dd");
	    this.jtfEqu = new FormattedTextFieldHelper(df);
	    this.jtfEqu.setText("AAAA-MM-JJ");
	    this.jtfAssu = new FormattedTextFieldHelper(df);
	    this.jtfAssu.setText("AAAA-MM-JJ");
	    this.jtfPhysInv = new FormattedTextFieldHelper(df);
	    this.jtfPhysInv.setText("AAAA-MM-JJ");
	    //Jamais �ditable (auto increment dans la base)
		this.jtfInv.setEnabled(false);
	}
	
	/*
	 * Affichage de la fen�tre selon le type (edition, affichage ou ajout)
	 * @param: String: type de formulaire (add, edit, view)
	 */
	public void equipementDisplay(String typeForm){
		  this.statusForm = typeForm;

		  //Le tableau des composant n'est jamais �ditable
		  this.jTableComposant.setEnabled(false);
		  this.jTableComposant.setForeground(new Color(106,153,200));
		
		  if (this.statusForm == "add") {
			  this.btEdit.setEnabled(false);
			  this.btView.setEnabled(false);
			  this.btSave.setEnabled(true);
			  this.setFieldsEnabled(true);
			  //D�sactive onglet composant (pas �ditable)
			  this.tab.setEnabledAt(3, false);

		  } else if(this.statusForm == "edit") {
			  this.btEdit.setEnabled(false);
			  this.btView.setEnabled(true);
			  this.btSave.setEnabled(true);
			  this.setFieldsEnabled(true);
			  //D�sactive onglet composant (pas �ditable)
			  this.tab.setEnabledAt(3, true);

		  } else if(this.statusForm == "view"){
			  this.btEdit.setEnabled(true);
			  this.btView.setEnabled(false);
			  this.btSave.setEnabled(false);
			  this.setFieldsEnabled(false); 
			  //D�sactive onglet composant (pas �ditable)
			  this.tab.setEnabledAt(3, true);
		  }
	}
	
	/*
	 * G�n�re l'ordre du focus
	 */
	public void setFocus(){
		MapFocusTraversalPolicyHelper focus = new MapFocusTraversalPolicyHelper();
		
		//focus selon les boutons activ�s dans la fen�tre
		focus.addComponent(this.btView, this.btView.isEnabled());
		focus.addComponent(this.btEdit, this.btEdit.isEnabled());
		focus.addComponent(this.tab, this.tab.isEnabled());
		
		focus.addComponent(this.listTechClass, this.listTechClass.isEnabled());
		
		focus.addComponent(this.jtfInv, this.jtfInv.isEnabled());
		focus.addComponent(this.jtfSerialNum, this.jtfSerialNum.isEnabled());
		focus.addComponent(this.jtfDesignation, this.jtfDesignation.isEnabled());
		focus.addComponent(this.jtfEqu, this.jtfEqu.isEnabled());
		focus.addComponent(this.jtfAssu, this.jtfAssu.isEnabled());
		focus.addComponent(this.jtfPhysInv, this.jtfPhysInv.isEnabled());
		
		focus.addComponent(this.listAssigned, this.listAssigned.isEnabled());
		focus.addComponent(this.jtfLocalisation, this.jtfLocalisation.isEnabled());
		focus.addComponent(this.jtfSup, this.jtfSup.isEnabled());
		focus.addComponent(this.listSupplier, this.listSupplier.isEnabled());
		focus.addComponent(this.jtfOrder, this.jtfOrder.isEnabled());
		focus.addComponent(this.jtfCost, this.jtfCost.isEnabled());
		
		focus.addComponent(this.jtfLocalisation, this.jtfLocalisation.isEnabled());
		focus.addComponent(this.jtfSup, this.jtfSup.isEnabled());
		focus.addComponent(this.listSupplier, this.listSupplier.isEnabled());
		focus.addComponent(this.jtfOrder, this.jtfOrder.isEnabled());
		focus.addComponent(this.jtfCost, this.jtfCost.isEnabled());
		
		focus.addComponent(this.jtfNameEqu, this.jtfNameEqu.isVisible());
		focus.addComponent(this.jtfSize, this.jtfSize.isVisible());
		focus.addComponent(this.listMaxSize, this.listMaxSize.isVisible());
		focus.addComponent(this.cbxDuplex, this.cbxDuplex.isVisible());
		focus.addComponent(this.cbxColor, this.cbxColor.isVisible());
		
		focus.addComponent(this.btSave, this.btSave.isEnabled());
		
		//ajoute le nouveau focus � la frame
		this.setFocusTraversalPolicy(focus);  
	}
	
	/*
	 * Active/d�sactive le visuel des champs selon le boolean pass�
	 * @param: boolean
	 */
	private void setFieldsEnabled(boolean isEnable){
		this.listTechClass.setEnabled(isEnable);
		this.jtfEqu.setEnabled(isEnable);
		this.jtfSerialNum.setEnabled(isEnable);
		this.jtfAssu.setEnabled(isEnable);
		this.jtfDesignation.setEnabled(isEnable);
		this.jtfPhysInv.setEnabled(isEnable);
		this.listAssigned.setEnabled(isEnable);
		this.listSupplier.setEnabled(isEnable);
		this.jtfLocalisation.setEnabled(false);
		this.jtfOrder.setEnabled(isEnable);
		this.jtfSup.setEnabled(false);
		this.jtfCost.setEnabled(isEnable);
    	this.jtfNameEqu.setEnabled(isEnable);
    	//this.jtfNamePrint.setEnabled(isEnable);
    	this.cbxDuplex.setEnabled(isEnable);
    	this.listMaxSize.setEnabled(isEnable);
    	this.cbxColor.setEnabled(isEnable);
    	this.jtfSize.setEnabled(isEnable);
    	this.txtAeraNote.setEnabled(isEnable);
    	this.txtAeraAssurance.setEnabled(isEnable);
    	
    	//Change le focus selon les champs activ�s
    	this.setFocus();
	}
	
	/*
	 * Cr�ation du contenu de la fen�tre 
	 */
	private void buildUI(){
		this.container.setLayout(new BorderLayout());
		//Ajout des boutons de gestion selon la page affich�e
		this.container.add(this.buttonDisplay(), BorderLayout.NORTH);
		this.container.add(this.tabDisplay(), BorderLayout.CENTER);
		this.container.add(this.buttonSaveDisplay(), BorderLayout.SOUTH);			   
	}
	
	/*
	 * Retourne les boutons � afficher
	 * @return: JPanel
	 */
	private JPanel buttonDisplay(){
		JPanel buttonsGroupManage = new JPanel();
	    buttonsGroupManage.setLayout(new BoxLayout(buttonsGroupManage, BoxLayout.LINE_AXIS));
	    
	    this.btView.setContentAreaFilled(false);
	    this.btView.setBorderPainted(false);
	    this.btView.addActionListener(this.equipementController);
	    
	    this.btEdit.setContentAreaFilled(false);
	    this.btEdit.setBorderPainted(false);
	    this.btEdit.addActionListener(this.equipementController);

	    buttonsGroupManage.add(this.btView);
	    buttonsGroupManage.add(this.btEdit);

	    return buttonsGroupManage;
	}
	
	/*
	 * Retourne l'affichage des onglets et leur contenu
	 * @return: JTabbedPane
	 */
	private JTabbedPane tabDisplay(){
	    //Cr�ation d'onglet pour deux type de recherche
	    this.tab.addTab("Objet", this.setTab(1));
	    this.tab.addTab("Remarques", this.setTab(2));
	    this.tab.addTab("Garantie", this.setTab(3));
	    this.tab.addTab("Composants", this.setTab(4));
	    return this.tab;
	}
	
	/*
	 * Retourne l'affichage du contenu de chaque onglet
	 * @param: filtre (1 => Onglet, 2 => Remarques, 3 => Garanties ou 4 => Composants)
	 * @return: JPanel  
	 */
	private JPanel setTab(int filtre){
		JPanel pan = new JPanel();		 
		pan.setBackground(Color.WHITE);  
		//Choix du type de filtre
	    switch(filtre){
	    case 1:
	    	pan = this.objectDisplay();
	    	break;
	    case 2:
	    	pan = this.noteDisplay();
	    	break;
	    case 3:
	    	pan = this.assuranceDisplay();
	    	break;
	    case 4: 
	    	pan = this.composantDisplay();
	    	break;
	    	default:
	    }
		
	    
	    return pan;
	}
	
	/*
	 * Renvoi du bouton de sauvegarde des donn�s
	 * @return JPanel
	 */
	private JPanel buttonSaveDisplay(){
		JPanel buttonSave = new JPanel();
		buttonSave.setLayout(new FlowLayout(FlowLayout.RIGHT));
		buttonSave.add(this.btSave);
		this.btSave.addActionListener(this.equipementController);
		
		return buttonSave;
	}
	
	/*
	 * Renvoi de l'affichage de l'onglet "Objet"
	 * @return: JPanel
	 */
	private JPanel objectDisplay(){
		JPanel pan = new JPanel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));

		//Classe technique
		JPanel techClass = new JPanel();

		techClass.setLayout(new FlowLayout(FlowLayout.LEFT));
		techClass.setBackground(Color.WHITE);
		techClass.setMaximumSize(new Dimension(800,30));
		JLabel labelTechClass = new JLabel("Classe technique");
		labelTechClass.setPreferredSize(new Dimension(120,20));
		this.listTechClass.setPreferredSize(new Dimension(223,20));
		this.listTechClass.addActionListener(this.equipementController);
		techClass.add(labelTechClass);
		techClass.add(this.listTechClass);
		
		//Information technique
		JPanel techInfo = new JPanel();
		techInfo.setLayout(new FlowLayout(FlowLayout.LEFT));
		techInfo.setBackground(Color.WHITE);
		Border border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY), "Informations techniques");
	    techInfo.setBorder(border);
	    
		JLabel labelInv = new JLabel("Num�ro d'inventaire");
		labelInv.setPreferredSize(new Dimension(135,20));
		this.jtfInv.setColumns(20);
		techInfo.add(labelInv);
		techInfo.add(this.jtfInv);
		
		JLabel labelEqu = new JLabel("R�ception mat�riel");
		labelEqu.setPreferredSize(new Dimension(160,20));
		labelEqu.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfEqu.setColumns(20);
		techInfo.add(labelEqu);
		techInfo.add(this.jtfEqu);
		
		JLabel labelSerialNum = new JLabel("Num�ro de s�rie");
		labelSerialNum.setPreferredSize(new Dimension(135,20));
		this.jtfSerialNum.setColumns(20);
		techInfo.add(labelSerialNum);
		techInfo.add(this.jtfSerialNum);
		
		JLabel labelAssu = new JLabel("Ech�ance de la garantie");
		labelAssu.setPreferredSize(new Dimension(160,20));
		labelAssu.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfAssu.setColumns(20);
		techInfo.add(labelAssu);
		techInfo.add(this.jtfAssu);
		
		JLabel labelDesignation = new JLabel("D�signation");
		labelDesignation.setPreferredSize(new Dimension(135,20));
		this.jtfDesignation.setColumns(20);
		techInfo.add(labelDesignation);
		techInfo.add(this.jtfDesignation);
		
		
		JLabel labelPhysInv = new JLabel("Inventaire physique");
		labelPhysInv.setPreferredSize(new Dimension(160,20));
		labelPhysInv.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfPhysInv.setColumns(20);
		techInfo.add(labelPhysInv);
		techInfo.add(this.jtfPhysInv);
		
		//Information technique
		JPanel adminInfo = new JPanel();
		adminInfo.setBackground(Color.WHITE);
		adminInfo.setLayout(new FlowLayout(FlowLayout.LEFT));
		
	    border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY), "Informations administratives");
	    adminInfo.setBorder(border);
	    
	    JLabel labelAssigned = new JLabel("Attribu� �");
		labelAssigned.setPreferredSize(new Dimension(135,20));
		this.listAssigned.setPreferredSize(new Dimension(223,20));	
		this.listAssigned.addActionListener(this.equipementController);
		adminInfo.add(labelAssigned);
		adminInfo.add(this.listAssigned);
		
	    JLabel labelSupplier = new JLabel("Fournisseur");
		labelSupplier.setPreferredSize(new Dimension(160,20));
		labelSupplier.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.listSupplier.setPreferredSize(new Dimension(223,20));
		adminInfo.add(labelSupplier);
		adminInfo.add(this.listSupplier);
		
	    JLabel labelLocalisation = new JLabel("Localisation");
		labelLocalisation.setPreferredSize(new Dimension(135,20));
		this.jtfLocalisation.setColumns(20);
		adminInfo.add(labelLocalisation);
		adminInfo.add(this.jtfLocalisation);
		
		
	    JLabel labelComNum = new JLabel("Num�ro de commande");
		labelComNum.setPreferredSize(new Dimension(160,20));
		labelComNum.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfOrder.setColumns(20);
		adminInfo.add(labelComNum);
		adminInfo.add(this.jtfOrder);
		
	    JLabel labelSup = new JLabel("Sup�rieur hi�rarchique");
		labelSup.setPreferredSize(new Dimension(135,20));
		this.jtfSup.setColumns(20);
		adminInfo.add(labelSup);
		adminInfo.add(this.jtfSup);
		
	    JLabel labelCost = new JLabel("Prix d'achat (CHF)");
		labelCost.setPreferredSize(new Dimension(160,20));
		labelCost.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfCost.setColumns(20);
		adminInfo.add(labelCost);
		adminInfo.add(this.jtfCost);
	    
		//Compl�ment technique
		this.techDetails.setBackground(Color.WHITE);
	
		this.techDetails.setLayout(new FlowLayout(FlowLayout.LEFT));
	    border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY), "Compl�ments techniques");
	    this.techDetails.setBorder(border);
	    this.techDetails.setPreferredSize(new Dimension (800, 50));
	    
	    this.labelNameEqu.setPreferredSize(new Dimension(120,20));
		this.jtfNameEqu.setColumns(20);
		this.techDetails.add(this.labelNameEqu);
		this.techDetails.add(this.jtfNameEqu);
		
		this.labelNamePrint.setPreferredSize(new Dimension(120,20));
		this.jtfNamePrint.setColumns(20);
		this.techDetails.add(this.labelNamePrint);
		this.techDetails.add(this.jtfNamePrint);
	    
		this.cbxDuplex.setBorder(new EmptyBorder(0, 20, 0, 200));
		this.cbxDuplex.setBackground(Color.WHITE);
		this.techDetails.add(this.cbxDuplex);
		
		this.labelMaxSize.setPreferredSize(new Dimension(120,20));
	    this.listMaxSize.setPreferredSize(new Dimension(223,20));
	    this.techDetails.add(this.labelMaxSize);
	    this.techDetails.add(this.listMaxSize);
		
		this.cbxColor.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.cbxColor.setBackground(Color.WHITE);
		this.techDetails.add(this.cbxColor);
		
		this.labelSize.setPreferredSize(new Dimension(120,20));
		this.jtfSize.setColumns(20);
		this.techDetails.add(this.labelSize);
		this.techDetails.add(this.jtfSize);
		
		pan.add(techClass);
		pan.add(techInfo);
		pan.add(adminInfo);
		pan.add(this.techDetails);
		
		this.setTechnicalSupplement("");
		
	    return pan;
	}
	
	/*
	 * Changement de l'affichage de la section "informations techniques"
	 * @param: String type de champs attendu � l'affichage (Ordinateur, Imprimante, Ecran ou autre (affiche rien) )
	 */
	public void setTechnicalSupplement(String type){
		Color color = new Color(0,0,0);
		Color borderColor = Color.LIGHT_GRAY;
		
	    if (type.equals("Laptop") || type.equals("Desktop") || type.equals("Serveur")) {
	    	this.labelNameEqu.setVisible(true);
	    	this.jtfNameEqu.setVisible(true);
	    	this.labelNamePrint.setVisible(false);
	    	this.jtfNamePrint.setVisible(false);
	    	this.cbxDuplex.setVisible(false);
	    	this.labelMaxSize.setVisible(false);
	    	this.listMaxSize.setVisible(false);
	    	this.cbxColor.setVisible(false);
	    	this.labelSize.setVisible(false);
	    	this.jtfSize.setVisible(false);
	    	
	    } else if (type.equals("Imprimante")) {
	    	this.labelNameEqu.setVisible(false);
	    	this.jtfNameEqu.setVisible(false);
	    	this.labelNamePrint.setVisible(true);
	    	this.jtfNamePrint.setVisible(true);
	    	this.cbxDuplex.setVisible(true);
	    	this.labelMaxSize.setVisible(true);
	    	this.listMaxSize.setVisible(true);
	    	this.cbxColor.setVisible(true);
	    	this.labelSize.setVisible(false);
	    	this.jtfSize.setVisible(false);

	    } else if (type.equals("Moniteur")) {
	    	this.labelNameEqu.setVisible(false);
	    	this.jtfNameEqu.setVisible(false);
	    	this.labelNamePrint.setVisible(false);
	    	this.jtfNamePrint.setVisible(false);
	    	this.cbxDuplex.setVisible(false);
	    	this.labelMaxSize.setVisible(false);
	    	this.listMaxSize.setVisible(false);
	    	this.cbxColor.setVisible(false);
	    	this.labelSize.setVisible(true);
	    	this.jtfSize.setVisible(true);
	    	
	    } else {
	    	color = new Color(106,153,200);
	    	borderColor = new Color(106,153,200);
	    	this.labelNameEqu.setVisible(false);
	    	this.jtfNameEqu.setVisible(false);
	    	this.labelNamePrint.setVisible(false);
	    	this.jtfNamePrint.setVisible(false);
	    	this.cbxDuplex.setVisible(false);
	    	this.labelMaxSize.setVisible(false);
	    	this.listMaxSize.setVisible(false);
	    	this.cbxColor.setVisible(false);
	    	this.labelSize.setVisible(false);
	    	this.jtfSize.setVisible(false);
	    }
	    
	    //Change la couleur du border des informations techniques si un champs est activ� dans le panel
    	TitledBorder border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, borderColor), "Compl�ments techniques");
    	border.setTitleColor(color);
    	this.techDetails.setBorder(border);
	}
	
	/*
	 * Renvoi du contenu de l'onglet "Remarques"
	 * @return: JPanel
	 */
	private JPanel noteDisplay(){
		JPanel pan = new JPanel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
		pan.add(this.txtAeraNote);
		
		return pan;
	}
	
	/*
	 * Renvoi du contenu de l'onglet "Garanties"
	 * @return: JPanel
	 */
	private JPanel assuranceDisplay(){
		JPanel pan = new JPanel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
		pan.add(this.txtAeraAssurance);
		
		return pan;
	}
	
	/*
	 * Renvoi du contenu de l'onglet "Composant"
	 * @return: JPanel
	 */
	private JPanel composantDisplay(){
		JPanel pan = new JPanel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));
		if (this.id != 0) {
			this.jTableComposant = this.createTable(); 
			pan.add(this.jTableComposant.getTableHeader(), BorderLayout.NORTH);
			pan.add(this.jTableComposant, BorderLayout.CENTER);
		}
		
		//Emp�che le d�placement des colones
		this.jTableComposant.getTableHeader().setReorderingAllowed(false);
		return pan;
	}
	
	public JButton getBtEdit(){
		return this.btEdit;
	}
	
	public JButton getBtView(){
		return this.btView;
	}
	
	public JButton getBtSave(){
		return this.btSave;
	}
	
	//Champs mat�riel
	@SuppressWarnings("rawtypes")
	public JComboBox getListTechClass(){
		return this.listTechClass;
	}
	
	public JTextField getJtfInv(){
		return this.jtfInv;
	}
	
	public JTextField getJtfEqu(){
		return this.jtfEqu;
	}
	
	public JTextField getJtfSerialNum(){
		return this.jtfSerialNum;
	}
	
	public JTextField getJtfAssu(){
		return this.jtfAssu;
	}
	
	public JTextField getJtfDesignation(){
		return this.jtfDesignation;
	}
	
	public JTextField getJtfPhysInv(){
		return this.jtfPhysInv;
	}
	
	@SuppressWarnings("rawtypes")
	public JComboBox getListAssigned(){
		return this.listAssigned;
	}
	@SuppressWarnings("rawtypes")
	public JComboBox getListSupplier(){
		return this.listSupplier;
	}
	
	public JTextField getJtfLocalisation(){
		return this.jtfLocalisation;
	}
	
	public JTextField getJtfSup(){
		return this.jtfSup;
	}
	
	public JTextField getJtfCost(){
		return this.jtfCost;
	}
	
	public JTextField getJtfOrder() {
		return this.jtfOrder;
	}
	
	public JTextField getJtfNameEqu(){
		return this.jtfNameEqu;
	}
	
	public JTextField getJtfSize(){
		return this.jtfSize;
	}
	
	public JTextField getJtfNamePrint(){
		return this.jtfNamePrint;
	}
	public JCheckBox getCbxDuplex(){
		return this.cbxDuplex;
	}
	
	@SuppressWarnings("rawtypes")
	public JComboBox getListMaxSize(){
		return this.listMaxSize;
	}
	public JCheckBox getCbxColor(){
		return this.cbxColor;
	}
	
	public JTextArea getTxtAreaAssurance() {
		return this.txtAeraAssurance;
	}
	
	public JTextArea getTxtAreaNote() {
		return this.txtAeraNote;
	}
	
	/*
	 * Ajoute les valeurs des composants de l'�quipement
	 */
	private void getFields() {
		ArrayList<HashMap<String,String>> rows = this.model.selectEquipement(this.id);

		HashSet<String> os = new HashSet<String>();
		HashSet<String> mem = new HashSet<String>();
		HashSet<String> nb = new HashSet<String>();
		HashSet<String> type = new HashSet<String>();
		HashSet<String> freq = new HashSet<String>();
		HashSet<String> bios = new HashSet<String>();
		HashSet<String> path = new HashSet<String>();
		HashSet<String> cap = new HashSet<String>();
		HashSet<String> lib = new HashSet<String>();
		HashSet<String> ip = new HashSet<String>();
		
		for (HashMap<String, String> r : rows) {
			fieldValues = r;
			
			os.add(this.fieldValues.get("composantOS"));
			mem.add(this.fieldValues.get("composantMem"));
			nb.add(this.fieldValues.get("composantCpuNb"));
			type.add(this.fieldValues.get("composantCpuType"));
			freq.add(this.fieldValues.get("composantCpuFreq"));
			bios.add(this.fieldValues.get("composantBios"));
			path.add(this.fieldValues.get("composantPath"));
			cap.add(this.fieldValues.get("composantCap"));
			lib.add(this.fieldValues.get("composantLib"));
			ip.add(this.fieldValues.get("composantIp"));

		}
		
		//txt composants			
		Object[][] d = {
			{"Syst�me d'exploitation",  getElements(os, "")},
			{"M�moire", getElements(mem, "Mo")},
			{"Nombre de CPU", getElements(nb, "")},
			{"Type du CPU", getElements(type, "")},
			{"Fr�quence du CPU", getElements(freq, "GHz")},
			{"Version du BIOS", getElements(bios, "")},
			{"Racine du disque", getElements(path, "")},
			{"Capacit� Disque(s)", getElements(cap, "Go")},
			{"Interface(s) r�seau", getElements(lib, "")},
			{"IP interface(s)", getElements(ip, "")}
		};	
		this.donnees = d;
	}

	/*
	 * Affiche les champs et leur contenu
	 */
	private void displayFields() {		
		//classe technique
		this.getListTechClass().setSelectedItem(this.fieldValues.get("listTechClass"));
				
		//informations techniques
		this.getJtfSerialNum().setText(this.fieldValues.get("jtfSerialNum"));
		this.getJtfInv().setText(this.fieldValues.get("jtfInv"));
		this.getJtfDesignation().setText(this.fieldValues.get("jtfDesignation"));		
		this.getJtfEqu().setText(this.fieldValues.get("jtfReception"));
		this.getJtfAssu().setText(this.fieldValues.get("jtfAssu"));
		this.getJtfPhysInv().setText(this.fieldValues.get("jtfPhysInv"));
					
		//informations administratives
		this.getListAssigned().setSelectedItem(this.fieldValues.get("listAssigned"));
		
		if (this.fieldValues.get("jtfLocalisation") == null) {
			this.getJtfLocalisation().setText(" - ");
		} else {
			this.getJtfLocalisation().setText(this.fieldValues.get("jtfLocalisation"));
		}
		
		if (this.fieldValues.get("jtfSup").startsWith("null")) {
			this.getJtfSup().setText("Pas de sup�rieur hi�rarchique");
		} else {
			this.getJtfSup().setText(this.fieldValues.get("jtfSup"));
		}
		this.getListSupplier().setSelectedItem(this.fieldValues.get("listSupplier"));
		this.getJtfOrder().setText(this.fieldValues.get("jtfOrder"));
		this.getJtfCost().setText(this.fieldValues.get("jtfCost"));
					
		//compl�ments techniques
		this.getJtfNameEqu().setText(this.fieldValues.get("jtfEqu"));
		this.getJtfSize().setText(this.fieldValues.get("jtfSize"));
		this.getJtfNamePrint().setText(this.fieldValues.get("jtfNamePrint"));
		this.getCbxDuplex().setSelected(Boolean.parseBoolean(this.fieldValues.get("cbxDuplex")));
		
		if(this.fieldValues.get("listMaxSize") == null) {
			this.getListMaxSize().setSelectedItem("S�lection");
		} else {
			this.getListMaxSize().setSelectedItem(this.fieldValues.get("listMaxSize"));
		}
		this.getCbxColor().setSelected(Boolean.parseBoolean(this.fieldValues.get("cbxColor")));

		//txt garantie
		this.getTxtAreaAssurance().setText(this.fieldValues.get("txtAeraAssurance"));
				
		//txt remarque
		this.getTxtAreaNote().setText(this.fieldValues.get("txtAeraNote"));
	}
	
	/*
	 * Retourne le JTable pour l'onglet composant 
	 * @return: JTable
	 */
	private JTable createTable() {
		JTable jTableComposant = new JTable(this.donnees, this.entetes) {
        	//Table pas �ditable
        	public boolean isCellEditable(int row, int column){
        		return false;
        	}
        };
        return jTableComposant;
	}
	
	/*
	 * V�rifie si l'�quipement est en cours d'utilisation
	 * @param: b
	 * @return: boolean
	 */
	public Boolean checkEditable(Boolean b) {
		Boolean isNotEditable = this.model.isEditable(this.id); //l'�quipement est-il en cours de modification? 
		//isNotEditable = true => equipement est d�j� en cours de modif
		//isNotEditable = false => equipement n'est d�j� en cours de modif et peut �tre modifi�	
		if (b) { //lock equipement 
			if (!isNotEditable) { //l'�quipement peut �tre modifi� (ModifEnCours = 0)
				this.model.setNoEditable(this.id, 1); //on le bloque pour les autres (ModifEnCours = 1)
			} else {
				JOptionPane.showMessageDialog(this, "Cet �quipement est d�j� en cours de modification", "Gestion IT - Fiche mat�riel", JOptionPane.ERROR_MESSAGE);
			}
		} else { //unlock equipement
			this.model.setNoEditable(this.id, 0);
		}
		return isNotEditable;
	}
	
	/*
	 * Retourne l'�quipement � MainView
	 * @return: EquipementModel
	 */
	public EquipementModel sendValue() {
		return this.equipementController.getEquipement();
	}
	
	/*
	 * Retourne un string compos� des valeurs uniques des champs composants
	 * @param: HashSet .> liste des valeurs uniques
	 * @param: String -> unit�s
	 * @return: string
	 */
	private String getElements(HashSet<String> hs, String unit) {
		String str = "";
		Iterator<String> it = hs.iterator();
        while(it.hasNext()) {
        	str += (String)it.next() + " " + unit + " ";
        }
        if (str.trim().equals("null") || str.trim().equals("null "+unit)) { 
        	str = "-";
        }
        return str;
	}
	
}
