package view;

import helpers.MapFocusTraversalPolicyHelper;
import helpers.TableExtend;
import helpers.TextFieldHelper;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import model.*;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import controller.MainController;

@SuppressWarnings("serial")
public class MainView extends JFrame{
	
	private MainModel model;
	private MainController mainController; 
	private JPanel container = new JPanel();
    private JTabbedPane tab = new JTabbedPane();
	private JTable tableAssigned;
	private JTable tableStock;
	
	/*Header tableau*/
    private String[] header = {
    					"No d'inventaire", 
    					"Fournisseur", 
    					"No de s�rie", 
    					"Nom machine",
    					"D�signation", 
    					"Attribu� �",
    					"Localisation",
    					"Mise en service",
    					"Ech�ance de garantie"};
    	
    /*Champs de filtre*/
    private JTextField jtfInv  = new TextFieldHelper("");
	private JTextField jtfMachineName = new TextFieldHelper("");
	private JTextField jtfAssigned = new TextFieldHelper("");
	private JTextField jtfSerialNumber = new TextFieldHelper("");
	private JTextField jtfDesignation = new TextFieldHelper("");
	private JTextField jtfLocalisation = new TextFieldHelper("");
	
	/*Boutons*/
	private JButton btSearch = new JButton("Rechercher");
	private JButton btAdd = new JButton(new ImageIcon(getClass().getResource("/view/add.jpg")));
	private JButton btView = new JButton(new ImageIcon(getClass().getResource("/view/view.jpg")));
	private JButton btEdit = new JButton(new ImageIcon(getClass().getResource("/view/edit.jpg")));
	private JButton btDelete = new JButton(new ImageIcon(getClass().getResource("/view/delete.jpg")));
	
	/*Menu*/
	private JMenuItem menuView = new JMenuItem("Afficher", KeyEvent.VK_A);
	private JMenuItem menuAdd = new JMenuItem("Ajouter", KeyEvent.VK_A);
	private JMenuItem menuEdit = new JMenuItem("Editer", KeyEvent.VK_D);
	private JMenuItem menuDelete = new JMenuItem("Supprimer", KeyEvent.VK_S);
	private JMenuItem menuAbout = new JMenuItem("A propos", KeyEvent.VK_P);
	private JMenuItem menuQuit = new JMenuItem("Quitter", KeyEvent.VK_Q);
	private JMenuItem menuSupplierManage = new JMenuItem("Gestion des fournisseurs");
	
	/*
	 * Constructeur
	 */
	public MainView(MainModel model) {
	    this.model = model;
		mainController = new MainController(this.model, this); 

	    //D�finition des propri�t�s de la fen�tre:
	    //Titre
	    this.setTitle("Gestion IT");
	    //Taille
	    this.setSize(1150, 600);
	    //Objet centrer
	    this.setLocationRelativeTo(null);
	    //Fermeture de la fen�tre par la croix
	    this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	    //Emp�che le redimensionnement de la fen�tre
	    this.setResizable(false);
	    //Laisse la fen�tre toujours au premier plan
	    this.setAlwaysOnTop(false);
	    //Assigne une icone � l'application
	    this.setIconImage(new ImageIcon(getClass().getResource("/view/gestionit.jpg")).getImage());
	    
	    this.buildUI();
	  
	    //le panel sera le content pane
	    this.setContentPane(this.container);       
	    
	    this.addWindowListener(this.mainController);
	    //fen�tre visible       
	    this.setVisible(true);
	}
	
	/*
	 * Construction de l'interface
	 */
	public void buildUI(){

	    //Ajout menu
	    this.setJMenuBar(this.generateMenu());
	    
	    this.container.setLayout(new BorderLayout());	
	    //D�finition de la couleur de fond
	    this.container.setBackground(new Color(240,240,240)); 
	    
	    //Ajout des panels dans l'interface
	    this.container.add(this.buttonDisplay(), BorderLayout.NORTH);
	    this.container.add(this.filterFiledsDisplay(), BorderLayout.CENTER);
	    this.container.add(this.tabDisplay(), BorderLayout.SOUTH);	   
	    
	}
	
	/*
	 * G�n�re l'ordre du focus
	 */
	private void setFocus(){
		MapFocusTraversalPolicyHelper focus = new MapFocusTraversalPolicyHelper();
		
		//focus selon les boutons activ�s dans la fen�tre
		focus.addComponent(this.btAdd, this.btAdd.isEnabled());
		focus.addComponent(this.btView, this.btView.isEnabled());
		focus.addComponent(this.btEdit, this.btEdit.isEnabled());
		focus.addComponent(this.btDelete, this.btDelete.isEnabled());

		focus.addComponent(this.jtfInv, this.jtfInv.isEnabled());
		focus.addComponent(this.jtfSerialNumber, this.jtfSerialNumber.isEnabled());
		focus.addComponent(this.jtfMachineName, this.jtfMachineName.isEnabled());
		focus.addComponent(this.jtfDesignation, this.jtfDesignation.isEnabled());
		focus.addComponent(this.jtfAssigned, this.jtfAssigned.isEnabled());
		focus.addComponent(this.jtfLocalisation, this.jtfLocalisation.isEnabled());
		
		focus.addComponent(this.btSearch, this.btSearch.isEnabled());
		focus.addComponent(this.tab, this.tab.isEnabled());
		
		//ajoute le nouveau focus � la frame
		this.setFocusTraversalPolicy(focus);  
	}
	
	/*
	 * Renvoi l'affichage des boutons images (ajout, afficher, �dition, suppression)
	 * @return: JPanel
	 */
	private JPanel buttonDisplay(){
		JPanel buttonsGroupManage = new JPanel();
	    buttonsGroupManage.setLayout(new BoxLayout(buttonsGroupManage, BoxLayout.LINE_AXIS));
	    
	    this.btAdd.setContentAreaFilled(false);
	    this.btAdd.setBorderPainted(false);
	    this.btAdd.addActionListener(this.mainController);
	    buttonsGroupManage.add(this.btAdd);
	    
	    this.btView.setContentAreaFilled(false);
	    this.btView.setBorderPainted(false);
	    this.btView.addActionListener(this.mainController);
	    buttonsGroupManage.add(this.btView);
	    
	    this.btEdit.setContentAreaFilled(false);
	    this.btEdit.setBorderPainted(false);
	    this.btEdit.addActionListener(this.mainController);
	    buttonsGroupManage.add(this.btEdit);
	    
	    this.btDelete.setContentAreaFilled(false);
	    this.btDelete.setBorderPainted(false);
	    this.btDelete.addActionListener(this.mainController);
	    buttonsGroupManage.add(this.btDelete);
	    
	    this.setButtonEnabled(false);
	    
	    return buttonsGroupManage;
	}
	
	/*
	 * Active/d�sactive l'affichage des boutons et des menus li�s
	 * @param: boolean
	 */
	public void setButtonEnabled(boolean isEnabled){
	    this.btView.setEnabled(isEnabled);
	    this.btEdit.setEnabled(isEnabled);
	    this.btDelete.setEnabled(isEnabled);
	    this.menuView.setEnabled(isEnabled);
	    this.menuEdit.setEnabled(isEnabled);
	    this.menuDelete.setEnabled(isEnabled);
	    //D�fini l'ordre du focus selon les boutons activ�s
	    this.setFocus();
	}
	
	/*
	 * Retourne les onglets avec leur contenu
	 * @return: JTabbedPane
	 */
	public JTabbedPane tabDisplay(){   
		this.tab.removeAll();
		
	    //Cr�ation d'onglet pour deux type de recherche
	    this.tab.addTab("Mat�riel distribu�", this.setTab(1, this.model.getEquipementAssignedList()));
	    this.tab.addTab("Mat�riel en stock", this.setTab(2, this.model.getEquipementStockList()));

	    this.tab.addChangeListener(mainController);
	    return this.tab;
	}
	
	/*
	 * G�n�re l'affichage des onglets et de leur contenus
	 * @param: ArrayList<EquipementModel> => liste : assign� ou stock�, int
	 * @return: JPanel => Retourne le panel correspondant � la liste
	 */
	public JPanel setTab(int index, ArrayList<EquipementModel> l){
		JPanel pan = new JPanel();
		pan.setLayout(new BoxLayout(pan, BoxLayout.PAGE_AXIS));

		if (index == 1) { //onglet Mat�riel assign�
			this.tableAssigned = new JTable() {
	        	//Table pas �ditable
	        	public boolean isCellEditable(int row, int column){
	        		return false;
	        	}
		    };
			
		    //modifie le contenu de la table � afficher
		    this.tableAssigned.setModel(new TableExtend(l, this.header));
		    
		    //tri des colonnes
		    this.tableAssigned.setAutoCreateRowSorter(true);
	
			//Emp�che le d�placement des colonnes
			this.tableAssigned.getTableHeader().setReorderingAllowed(false);
			this.tableAssigned.setPreferredScrollableViewportSize(new Dimension(1000,360));
			this.tableAssigned.addMouseListener(mainController);	
	        
	        pan.add(new JScrollPane(this.tableAssigned));
			
		} else { //onglet Mat�riel en stock
			this.tableStock = new JTable() {
	        	//Table pas �ditable
	        	public boolean isCellEditable(int row, int column){
	        		return false;
	        	}
			};
		
		    //modifie le contenu de la table � afficher
		    this.tableStock.setModel(new TableExtend(l, this.header));	
		    
		    //tri des colonnes
		    this.tableStock.setAutoCreateRowSorter(true);
	
			//Emp�che le d�placement des colonnes
			this.tableStock.getTableHeader().setReorderingAllowed(false);
			this.tableStock.setPreferredScrollableViewportSize(new Dimension(1000,360));
			this.tableStock.addMouseListener(mainController);	
	        
	        pan.add(new JScrollPane(this.tableStock));		
		}
 
	    return pan;
	}
	
	/*
	 * Retourne les champs de filtres
	 * @return: JPanel
	 */
	private JPanel filterFiledsDisplay(){
		JPanel container = new JPanel();
		container.setLayout(new BoxLayout(container, BoxLayout.PAGE_AXIS));
		//Cr�ation panel du haut qui correspond au champs de filtre de recherche
	    Border border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY), "Recherche de mat�riel par");
	    container.setBorder(border);
	    
	    //Premi�re ligne
		JPanel pan = new JPanel(new FlowLayout(FlowLayout.LEFT));	
		pan.setMaximumSize(new Dimension(1150,450));
		
		JLabel labelInv = new JLabel("No d'inventaire");
		labelInv.setPreferredSize(new Dimension(90,20));
		this.jtfInv.setColumns(15);
		pan.add(labelInv);
		pan.add(this.jtfInv);
		
	    JLabel labelMachineName = new JLabel("Nom machine");
	    labelMachineName.setPreferredSize(new Dimension(100,20));
	    labelMachineName.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfMachineName.setColumns(15);
	    pan.add(labelMachineName);
	    pan.add(this.jtfMachineName);
		
		JLabel labelAssigned = new JLabel("Attribu� �");
		labelAssigned.setPreferredSize(new Dimension(90,20));
		labelAssigned.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfAssigned.setColumns(15);
		pan.add(labelAssigned);
		pan.add(this.jtfAssigned);
		
	    //Deuxi�me ligne
		JPanel pan2 = new JPanel(new FlowLayout(FlowLayout.LEFT));	
		JLabel labelSerialNumber = new JLabel("No de s�rie");
		labelSerialNumber.setPreferredSize(new Dimension(90,20));
		this.jtfSerialNumber.setColumns(15);
		pan2.add(labelSerialNumber);
		pan2.add(this.jtfSerialNumber);
		
		JLabel labelDesignation = new JLabel("D�signation");
		labelDesignation.setPreferredSize(new Dimension(100,20));
		labelDesignation.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfDesignation.setColumns(15);
		pan2.add(labelDesignation);
		pan2.add(this.jtfDesignation);
		
		JLabel labelLocalisation = new JLabel("Localisation");
		labelLocalisation.setPreferredSize(new Dimension(90,20));
		labelLocalisation.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfLocalisation.setColumns(15);
		pan2.add(labelLocalisation);
		pan2.add(this.jtfLocalisation);
		
		MainController mainController = new MainController(this.model, this); 
		this.btSearch.addActionListener(mainController);
		pan2.add(this.btSearch);
		
		//Ajout de la premi�re et deuxi�me ligne dans l'interface	
		container.add(pan);
		container.add(pan2);
		
		return container;
	}
	
	/*
	 * Renvoi le menu de l'application
	 * @return: JMenuBar
	 */
	private JMenuBar generateMenu(){
		JMenuBar menuBar = new JMenuBar();
		  
		//Menu Fichier: afficher, quitter
		JMenu file = new JMenu("Fichier");
		file.setMnemonic( KeyEvent.VK_F);
		
		this.menuView.addActionListener(this.mainController);
		this.menuView.setAccelerator(KeyStroke.getKeyStroke("control F"));
		file.add(this.menuView);
		
		this.menuQuit.addActionListener(this.mainController);
		this.menuQuit.setAccelerator(KeyStroke.getKeyStroke("control Q"));
		file.add(this.menuQuit);

		menuBar.add(file);

		//Menu Edition: Ajouter , Editer , Supprimer
		JMenu edit = new JMenu("Edition");
		edit.setMnemonic( KeyEvent.VK_E);

		this.menuAdd.addActionListener(this.mainController);
		this.menuAdd.setAccelerator(KeyStroke.getKeyStroke("control A"));
		edit.add(this.menuAdd);
		
		this.menuEdit.addActionListener(this.mainController);
		this.menuEdit.setAccelerator(KeyStroke.getKeyStroke("control E"));
		edit.add(this.menuEdit);
		
		this.menuDelete.addActionListener(this.mainController);
		this.menuDelete.setAccelerator(KeyStroke.getKeyStroke("control D"));
		edit.add(this.menuDelete);

		menuBar.add(edit);
		
		//Menu Outils: Gestion des fournisseurs
		JMenu menuTool = new JMenu("Outil");
		menuTool.setMnemonic( KeyEvent.VK_O);
		
		menuTool.add(this.menuSupplierManage);
		this.menuSupplierManage.addActionListener(this.mainController);
		this.menuSupplierManage.setMnemonic( KeyEvent.VK_G);
		this.menuSupplierManage.setAccelerator(KeyStroke.getKeyStroke("control G"));


		menuBar.add(menuTool);
		
		//Menu Aide: A propos
		JMenu help = new JMenu("?");
		help.setMnemonic( KeyEvent.VK_H);

		this.menuAbout.addActionListener(this.mainController);
		help.add(this.menuAbout);

		menuBar.add(help);

		//Cr�er les menus
		return menuBar;
	  }
	
	/*
	 * Getters
	 */
	public JTabbedPane getTabbedPane() {
		return this.tab;
	}

	public JTable getTableStock() {
		return this.tableStock;
	}
	  
	public JTable getTableAssigned() {
		return this.tableAssigned;
	}
	    
	public JTextField getJtfInv() {
		return this.jtfInv;
	}
	  
	public JTextField getJtfMachineName() {
		return this.jtfMachineName;
	}
	  
	public JTextField getJtfAssigned() {
		return this.jtfAssigned;
	}
	  
	public JTextField getJtfSerialNumber() {
		return this.jtfSerialNumber;
	}
	  
	public JTextField getJtfDesignation() {
		return this.jtfDesignation;
	}
	  
	public JTextField getJtfLocalisation() {
		return this.jtfLocalisation;
	}
		
	public JButton getBtSearch() {
		return this.btSearch;
	}
	  
	public JButton getBtAdd() {
		return this.btAdd;
	}
	  
	public JButton getBtView() {
		return this.btView;
	}
	  
	public JButton getBtEdit() {
		return this.btEdit;
	}
	  
	public JButton getBtDelete() {
		return this.btDelete;
	}
		
	public JMenuItem getMenuView() {
		return this.menuView;
	}
	  
	public JMenuItem getMenuAdd() {
		return this.menuAdd;
	}
	  
	public JMenuItem getMenuEdit() {
		return this.menuEdit;
	}
	  
	public JMenuItem getMenuDelete() {
		return this.menuDelete;
	}
	  
	public JMenuItem getMenuAbout() {
		return this.menuAbout;
	}
	  
	public JMenuItem getMenuQuit() {
		return this.menuQuit;
	}
	  
	public JMenuItem getMenuSupplierManage() {
		return this.menuSupplierManage;
	}
	
}
