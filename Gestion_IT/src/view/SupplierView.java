package view;

import helpers.*;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import controller.SupplierController;
import model.SupplierModel;

@SuppressWarnings("serial")
public class SupplierView extends JDialog {
	private SupplierModel model;
	private SupplierController supplierController; 
	private String statusForm;
	private JPanel container = new JPanel();
	private JTable table;
	
	/*Header tableau*/
    private String[] header = {
    					"Id",
    					"Raison sociale", 
    					"Rue", 
    					"Num�ro", 
    					"NPA",
    					"Ville", 
    					"Pays",
    					"T�l�phone",
    					"E-mail"};
	
	//Boutons
	private JButton btAdd = new JButton(new ImageIcon(getClass().getResource("/view/add.jpg")));
	private JButton btEdit = new JButton(new ImageIcon(getClass().getResource("/view/edit.jpg")));
	private JButton btDelete = new JButton(new ImageIcon(getClass().getResource("/view/delete.jpg")));
	private JButton btSave = new JButton("Enregistrer");

	//Champs
	private JTextField jtfCompanyName = new TextFieldHelper("");
	private JTextField jtfCity = new TextFieldHelper("");
	private JTextField jtfRoad = new TextFieldHelper("");
	private JTextField jtfCountry = new TextFieldHelper("");
	private JTextField jtfNumber = new TextFieldHelper("");
	private JTextField jtfPhone = new TextFieldHelper("");
	private JTextField jtfNPA = new TextFieldHelper("");
	private JTextField jtfEmail = new TextFieldHelper("");

	// Constructeur
	public SupplierView(MainView frame, SupplierModel model) {
		super(frame, true);
		this.model = model;
		this.supplierController = new SupplierController(this.model, this);
		this.statusForm = "view";
		
		 //D�finition des propri�t�s de la fen�tre:
	    //Titre
	    this.setTitle("Gestion IT - Gestion des fournisseurs");
	    //Taille
	    this.setSize(800, 600);
	    //Fermeture de la fen�tre par la croix
	    this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    //Objet centrer
	    this.setLocationRelativeTo(null);
	    //Emp�che le redimensionnement de la fen�tre
	    this.setResizable(false);
	    //Laisse la fen�tre toujours au premier plan
	    this.setAlwaysOnTop(false);
	    
	    this.buildUI();
	   	    
	    this.supplierDisplay(this.statusForm);
	    
	    //le panel sera le content pane
	    this.setContentPane(this.container);  
	    
	    //fen�tre visible       
	    this.setVisible(true);	
	}
	
	/*
	 * Cr�ation du contenu de la fen�tre 
	 */
	private void buildUI() {
		this.container.setLayout(new BorderLayout());
		//Ajout des boutons de gestion selon la page affich�e
		this.container.add(this.buttonDisplay(), BorderLayout.NORTH);
		this.container.add(this.supplierFieldsDisplay(), BorderLayout.CENTER);
		this.container.add(this.tableDisplay(), BorderLayout.SOUTH);			   
	}
	
	/*
	 * Affichage de la fen�tre selon le type (edition, affichage ou ajout)
	 * @param: String => type de formulaire (add, edit, view)
	 */
	public void supplierDisplay(String typeForm){
		  this.statusForm = typeForm;
		  
		  if (this.statusForm == "view"){
			  this.setFieldsEnabled(false);  
		  } else {
			  this.setFieldsEnabled(true);  
		  }
	}
	
	/*
	 * Active/d�sactive l'affichage des boutons
	 * @param: boolean
	 */
	public void setButtonEnabled(boolean isEnabled){
	    this.btEdit.setEnabled(isEnabled);
	    this.btDelete.setEnabled(isEnabled);

	    //Change l'ordre du focus
	    this.setFocus();
	}
	
	/*
	 * Active/d�sactive les champs du formulaire
	 * @param: boolean
	 */
	private void setFieldsEnabled(boolean isEnable){
		this.jtfCompanyName.setEnabled(isEnable);
		this.jtfCity.setEnabled(isEnable);
		this.jtfRoad.setEnabled(isEnable);
		this.jtfCountry.setEnabled(isEnable);
		this.jtfNumber.setEnabled(isEnable);
		this.jtfPhone.setEnabled(isEnable);
		this.jtfNPA.setEnabled(isEnable);
		this.jtfEmail.setEnabled(isEnable);
		this.btSave.setEnabled(isEnable);
	}
	
	/*
	 * Renvoi les boutons � afficher
	 * @return: JPanel
	 */
	private JPanel buttonDisplay(){
		JPanel buttonsGroupManage = new JPanel();
	    buttonsGroupManage.setLayout(new BoxLayout(buttonsGroupManage, BoxLayout.LINE_AXIS));
	    
	    this.btAdd.setContentAreaFilled(false);
	    this.btAdd.setBorderPainted(false);
	    this.btAdd.addActionListener(this.supplierController);
	    
	    this.btEdit.setContentAreaFilled(false);
	    this.btEdit.setBorderPainted(false);
	    this.btEdit.addActionListener(this.supplierController);

	    this.btDelete.setContentAreaFilled(false);
	    this.btDelete.setBorderPainted(false);
	    this.btDelete.addActionListener(this.supplierController); 
	    
	    buttonsGroupManage.add(this.btAdd);
	    buttonsGroupManage.add(this.btEdit);
	    buttonsGroupManage.add(this.btDelete);

	    this.setButtonEnabled(false);
	    
	    return buttonsGroupManage;
	}
	
	/*
	 * G�n�re l'ordre du focus
	 */
	private void setFocus(){
		MapFocusTraversalPolicyHelper focus = new MapFocusTraversalPolicyHelper();
		
		//focus selon les boutons activ�s dans la fen�tre
		focus.addComponent(this.btAdd, this.btAdd.isEnabled());
		focus.addComponent(this.btEdit, this.btEdit.isEnabled());
		focus.addComponent(this.btDelete, this.btDelete.isEnabled());

		focus.addComponent(this.jtfCompanyName, this.jtfCompanyName.isEnabled());
		focus.addComponent(this.jtfRoad, this.jtfRoad.isEnabled());
		focus.addComponent(this.jtfNumber, this.jtfNumber.isEnabled());
		focus.addComponent(this.jtfNPA, this.jtfNPA.isEnabled());

		focus.addComponent(this.jtfCity, this.jtfCity.isEnabled());
		focus.addComponent(this.jtfCountry, this.jtfCountry.isEnabled());
		focus.addComponent(this.jtfPhone, this.jtfPhone.isEnabled());
		focus.addComponent(this.jtfEmail, this.jtfEmail.isEnabled());
		
		focus.addComponent(this.btSave, this.btSave.isEnabled());
		
		//ajoute le nouveau focus � la frame
		this.setFocusTraversalPolicy(focus);  
	}
	
	/*
	 * Renvoi de l'affichage de l'onglet "Objet"
	 * @return: JPanel
	 */
	private JPanel supplierFieldsDisplay(){
		JPanel pan = new JPanel();
		pan.setLayout(new FlowLayout(FlowLayout.LEFT));
		
		Border border = BorderFactory.createTitledBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.LIGHT_GRAY), "Fiche fournisseur");
		pan.setBorder(border);		

		
		JPanel pan1 = new JPanel(new FlowLayout(FlowLayout.LEFT));	

		JLabel labelCompanyName = new JLabel("Raison sociale");
		labelCompanyName.setPreferredSize(new Dimension(100,20));
		this.jtfCompanyName.setColumns(20);
		pan1.add(labelCompanyName);
		pan1.add(this.jtfCompanyName);
		
		JLabel labelCity = new JLabel("Ville");
		labelCity.setPreferredSize(new Dimension(100,20));
		labelCity.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfCity.setColumns(20);
		pan1.add(labelCity);
		pan1.add(this.jtfCity);
		
		
		JPanel pan2 = new JPanel(new FlowLayout(FlowLayout.LEFT));	

		JLabel labelRoad = new JLabel("Rue");
		labelRoad.setPreferredSize(new Dimension(100,20));
		this.jtfRoad.setColumns(20);
		pan2.add(labelRoad);
		pan2.add(this.jtfRoad);
		
		JLabel labelCountry = new JLabel("Pays");
		labelCountry.setPreferredSize(new Dimension(100,20));
		labelCountry.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfCountry.setColumns(20);
		pan2.add(labelCountry);
		pan2.add(this.jtfCountry);

		
		JPanel pan3 = new JPanel(new FlowLayout(FlowLayout.LEFT));	
		
		JLabel labelNumber = new JLabel("Num�ro");
		labelNumber.setPreferredSize(new Dimension(100,20));
		this.jtfNumber.setColumns(20);
		pan3.add(labelNumber);
		pan3.add(this.jtfNumber);
		
		JLabel labelPhone = new JLabel("T�l�phone");
		labelPhone.setPreferredSize(new Dimension(100,20));
		labelPhone.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfPhone.setColumns(20);
		pan3.add(labelPhone);
		pan3.add(this.jtfPhone);

		
		JPanel pan4 = new JPanel(new FlowLayout(FlowLayout.LEFT));	
		
		JLabel labelNPA = new JLabel("NPA");
		labelNPA.setPreferredSize(new Dimension(100,20));
		this.jtfNPA.setColumns(20);
		pan4.add(labelNPA);
		pan4.add(this.jtfNPA);
		
		JLabel labelEmail = new JLabel("E-mail");
		labelEmail.setPreferredSize(new Dimension(100,20));
		labelEmail.setBorder(new EmptyBorder(0, 20, 0, 0));
		this.jtfEmail.setColumns(20);
		pan4.add(labelEmail);
		pan4.add(this.jtfEmail);
		
		pan4.add(this.btSave);
		this.btSave.addActionListener(this.supplierController); 
		
		pan.add(pan1);
		pan.add(pan2);
		pan.add(pan3);
		pan.add(pan4);

	    return pan;
	}
	
	/*
	 * Retourne le visuel du tableau de fournisseur
	 * @return: Jpanel
	 */
	public JPanel tableDisplay(){
		JPanel pan = new JPanel();

		ArrayList<SupplierModel> list = this.model.getSuppliersList();
		
		//Cr�ation du tableau qui ne sera pas �ditable
        this.table = new JTable(new SupplierTableExtend(list, this.header)){
        	//Table pas �ditable
        	public boolean isCellEditable(int row, int column){
        		return false;
        	}
        };
        
		//Emp�che le d�placement des collones
		this.table.getTableHeader().setReorderingAllowed(false);
		this.table.setPreferredScrollableViewportSize(new Dimension(800,300));
		this.table.addMouseListener(supplierController);
        
        pan.add(new JScrollPane(this.table));
		return pan;
	}
	
	
	//Boutons
	public JButton getBtAdd(){
		return this.btAdd;
	}
	public JButton getBtEdit(){
		return this.btEdit;
	}
	public JButton getBtDelete(){
		return this.btDelete;
	}
	public JButton getBtSave(){
		return this.btSave;
	}

	//Champs
	public JTextField getJtfCompanyName(){
		return this.jtfCompanyName;
	}
	public JTextField getJtfCity(){
		return this.jtfCity;
	}
	public JTextField getJtfRoad(){
		return this.jtfRoad;
	}
	public JTextField getJtfCountry(){
		return this.jtfCountry;
	}
	public JTextField getJtfNumber(){
		return this.jtfNumber;
	}
	public JTextField getJtfPhone(){
		return this.jtfPhone;
	}
	public JTextField getJtfNPA(){
		return this.jtfNPA;
	}
	public JTextField getJtfEmail(){
		return this.jtfEmail;
	}
	
	//Table
	public JTable getTable(){
		return this.table;
	}

	
	/*
	 * Affichage des champs rempli par les donn�es d'un fournisseur
	 * @param: int => id du fournisseur s�lectionn�
	 */
	public void displayFields(int id) {
		ArrayList<SupplierModel> list = this.model.getSuppliersList();
		
		// on r�cup�re l'id de la liste qui correspond � notre id fournisseu
		int index = this.supplierController.getSupplierFromList(list, id);
		
		this.getJtfCompanyName().setText(list.get(index).getRaisonSociale());
		this.getJtfCity().setText(list.get(index).getVille());
		this.getJtfRoad().setText(list.get(index).getRue());
		this.getJtfCountry().setText(list.get(index).getPays());
		if (list.get(index).getNo() == 0) {
			this.getJtfNumber().setText("");
		} else {
			this.getJtfNumber().setText(String.valueOf(list.get(index).getNo()));
		}
		this.getJtfPhone().setText(list.get(index).getTel());
		this.getJtfNPA().setText(String.valueOf(list.get(index).getNpa()));
		this.getJtfEmail().setText(list.get(index).getEmail());
	}
	
	/*
	 * Vide tous les champs pour l'ajout d'un nouveau fournisseur
	 */
	public void clearFields() {
		this.getJtfCompanyName().setText("");
		this.getJtfCity().setText("");
		this.getJtfRoad().setText("");
		this.getJtfCountry().setText("");
		this.getJtfNumber().setText("");
		this.getJtfPhone().setText("");
		this.getJtfNPA().setText("");
		this.getJtfEmail().setText("");
	}
}
