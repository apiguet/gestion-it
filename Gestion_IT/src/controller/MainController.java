package controller;

import helpers.TableExtend;

import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.TableRowSorter;
import model.EquipementModel;
import model.SupplierModel;
import view.EquipementView;
import view.SupplierView;
import controller.EquipementController;
import model.MainModel;
import view.MainView;

public class MainController implements ActionListener, WindowListener, MouseListener, ChangeListener {
	private MainModel model; 
	private MainView view;
	private Object id = 0;
	private int sourceTab = 0;

	/*
	 * Constructeur
	 */
	public MainController (MainModel model, MainView view) {
		this.model = model;
		this.view = view;
	}

	/*
	 * Gestion des boutons et des menus s�lectionn�s
	 */
	public void actionPerformed (ActionEvent arg0) {
		Object source=arg0.getSource();
		
		//Quel bouton a �t� press�?
		if (source == view.getBtSearch()) {
			// filtre
			JTable tabA = this.view.getTableAssigned(); 
			TableRowSorter<TableExtend> sorterA = new TableRowSorter<TableExtend>((TableExtend) tabA.getModel());
			tabA.setRowSorter(sorterA);
			
			JTable tabS = this.view.getTableStock(); 
			TableRowSorter<TableExtend> sorterS = new TableRowSorter<TableExtend>((TableExtend) tabS.getModel());
			tabS.setRowSorter(sorterS);
			
			ArrayList<RowFilter<Object,Object>> filters = new ArrayList<RowFilter<Object,Object>>(6);
			filters.add(RowFilter.regexFilter(this.view.getJtfInv().getText(), 0));
			filters.add(RowFilter.regexFilter(this.view.getJtfMachineName().getText(), 3));
			filters.add(RowFilter.regexFilter(this.view.getJtfAssigned().getText(), 5));
			filters.add(RowFilter.regexFilter(this.view.getJtfSerialNumber().getText(), 2));
			filters.add(RowFilter.regexFilter(this.view.getJtfDesignation().getText(), 4));
			filters.add(RowFilter.regexFilter(this.view.getJtfLocalisation().getText(), 6));
			
			RowFilter<Object,Object> f = RowFilter.andFilter(filters);
			sorterA.setRowFilter(f);
			sorterS.setRowFilter(f);
			
			//D�sactive les boutons.
			this.view.setButtonEnabled(false);

		} else if(source == this.view.getMenuQuit()) {
			System.exit(0);
		
		} else if(source == this.view.getMenuSupplierManage()) {
			SupplierModel supplierModel = new SupplierModel();
			SupplierView supplierView = new SupplierView(this.view, supplierModel);
			@SuppressWarnings("unused")
			SupplierController supplierController = new SupplierController(supplierModel, supplierView);
		
		} else if(source == this.view.getMenuAbout()){
			JOptionPane.showMessageDialog(this.view, "Ce programme a �t� d�velopp� par Fr�d�ric Medana, M�lanie Scyboz et Aude Piguet\n Version 1.0", "Gestion IT - A Propos", JOptionPane.INFORMATION_MESSAGE);
		
		} else if(source == this.view.getBtView() || source == this.view.getMenuView()){
			EquipementModel equipementModel = new EquipementModel();
			EquipementView equipementView = new EquipementView(this.view, equipementModel, "view", (Integer) this.id);
			@SuppressWarnings("unused")
			EquipementController equipementController = new EquipementController(equipementModel,equipementView);
			
		} else if(source == this.view.getBtAdd() || source == this.view.getMenuAdd()){
			EquipementModel equipementModel = new EquipementModel();
			EquipementView equipementView = new EquipementView(this.view, equipementModel, "add", 0);
			@SuppressWarnings("unused")
			EquipementController equipementController = new EquipementController(equipementModel,equipementView);
			
			EquipementModel em = equipementView.sendValue();	
			if (em != null) {	
				JTable t;
				if (em.getnomAttribueA().equals("-")) { //on ajout� un �quipement en stock
					t = this.view.getTableStock();
					this.view.getTabbedPane().setSelectedIndex(1);
				} else { //on ajout� un �quipement assign� � un employ�
					t = this.view.getTableAssigned();
					this.view.getTabbedPane().setSelectedIndex(0);
				}
				TableExtend model = (TableExtend) t.getModel();
				model.addEquipement(em);
			}
			
		} else if(source == this.view.getBtEdit() || source == this.view.getMenuEdit()){
			Boolean isNotEditable = this.model.isEditable((Integer) this.id);
			if (!isNotEditable) {	
				this.editAction("edit");
			} else {
				JOptionPane.showMessageDialog(this.view, "Cet �quipement est d�j� en cours de modification", "Gestion IT - Fiche mat�riel", JOptionPane.ERROR_MESSAGE);
			}
		
		} else if(source == this.view.getBtDelete() || source == this.view.getMenuDelete()){
			//Suppression d'un �quipement
			int rep = JOptionPane.showConfirmDialog(this.view, "Etes-vous s�r de vouloir supprimer cet enregistrement?", "Gestion IT", JOptionPane.YES_NO_OPTION);
			if (rep == 0) {
				int result = this.model.deleteEquipement((Integer) this.id);
				if (result == 0) {
					JOptionPane.showMessageDialog(this.view, "La requ�te n'a pas pu s'effectuer", "Gestion IT", JOptionPane.ERROR_MESSAGE);
				
				} else {
					if (sourceTab == 0) { //on est sur l'onglet Mat�riel assign�
						int index = getEquipementFromList(this.model.getEquipementAssignedList(), (Integer) this.id);
						JTable t = this.view.getTableAssigned();
						TableExtend model = (TableExtend) t.getModel();
						model.removeEquipement(index);
					
					} else { //on est sur l'onglet Mat�riel en stock
						int index = getEquipementFromList(this.model.getEquipementStockList(), (Integer) this.id);
						JTable t = this.view.getTableStock();
						TableExtend model = (TableExtend) t.getModel();
						model.removeEquipement(index);
					}
				}
				this.view.setButtonEnabled(false);
			}
		}
	
	}
	
	
	public void windowActivated(WindowEvent arg0) {
		this.view.setEnabled(true);
	}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {
		System.exit(0);
	}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	

	/*
	 * Gestion du clic sur les lignes des tableaux du main
	 */
	public void mouseClicked(MouseEvent arg0) {	
		//On active les boutons de modification 
		this.view.setButtonEnabled(true);

		JTable table;
		if (this.sourceTab == 0) {
			table = this.view.getTableAssigned();
		} else {
			table = this.view.getTableStock();
		}
        Point p = arg0.getPoint();
        int row = table.rowAtPoint(p);
        this.id = table.getValueAt(row, 0);
        
        table.requestFocus();
         
        //redonne le focus sur le premier bouton
        this.view.getBtAdd().requestFocus();
        
		//Test double clique
		if(arg0.getClickCount() == 2){
			this.editAction("view");
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}

	/*
	 * Ev�nement: changement d'onglet
	 */
	public void stateChanged(ChangeEvent arg0) {

		//Changement visuel � faire selon quel onglet est s�lectionn�
		JTabbedPane sourceTabbedPane = (JTabbedPane) arg0.getSource();
		int index = sourceTabbedPane.getSelectedIndex();
		boolean isEnable = true;
		
		JTable table;
		if (index == 1) { //onglet Mat�riel en stock
			isEnable = false;
			this.sourceTab = 1;
			table = this.view.getTableAssigned();

		} else { //onglet Mat�riel assign�
			this.sourceTab = 0;
			table = this.view.getTableStock();
		}
		
		table.getSelectionModel().clearSelection();
		
		//On active / d�sactive les champs (attribu� � et localisation) selon l'onglet
		this.view.getJtfAssigned().setEnabled(isEnable);
		this.view.getJtfLocalisation().setEnabled(isEnable);
		
		//on active/d�sactive les boutons
		this.view.setButtonEnabled(false);
	}

	/*
	 * Action lorsque l'on clique sur edit
	 * @param: String 
	 */
	public void editAction(String mode) {
		//ouvre la fen�tre d'affichage du mat�riel
		EquipementModel equipementModel = new EquipementModel();
		EquipementView equipementView = new EquipementView(this.view, equipementModel, mode, (Integer) id);
		@SuppressWarnings("unused")
		EquipementController equipementController = new EquipementController(equipementModel,equipementView);
		
		EquipementModel em = equipementView.sendValue();	
		if (em != null) {	
			
			 JTable tableStock = this.view.getTableStock();
			 TableExtend modelStock = (TableExtend) tableStock.getModel();
			 
			 JTable tableAssigned = this.view.getTableAssigned();
			 TableExtend modelAssigned = (TableExtend) tableAssigned.getModel(); 
	
			if (sourceTab == 0) { // on est sur l'onglet Mat�riel assign�
				 if (em.getnomAttribueA().equals("-")) { // on a mis en stock un �quipement qui �tait assign� => il faut le changer de liste		 
					 modelStock.addEquipement(em);
					 
					 int index = getEquipementFromList(this.model.getEquipementAssignedList(), (Integer) this.id);
					 modelAssigned.removeEquipement(index);
					 
				 } else { // on le modifie
					 int index = getEquipementFromList(this.model.getEquipementAssignedList(), (Integer) this.id);
					 modelAssigned.updateEquipement(index, em);
				 }
				
			} else { // on est sur l'onglet Mat�riel en stock
				 if (em.getnomAttribueA().equals("-")) { // on le modifie  
					 int index = getEquipementFromList(this.model.getEquipementStockList(), (Integer) this.id);
					 modelStock.updateEquipement(index, em);
					 
				 } else {  // on a assign� un �quipement qui �tait en stock => il faut le changer de liste
					 modelAssigned.addEquipement(em);
					 
					 int index = getEquipementFromList(this.model.getEquipementStockList(), (Integer) this.id);
					 modelStock.removeEquipement(index);
				 }

			}
		}
	}
	
	/*
	 * on r�cup�re l'id de la liste qui correspond � notre id �quipement
	 * @param: ArrayList, int
	 * @return: id dans la liste
	*/
	private int getEquipementFromList(ArrayList<EquipementModel> list, int index) {
		 int i = 0;
		 for (EquipementModel e : list) {
			 if (e.getNoInventaire() == index) {
				 i = list.indexOf(e);
				 break;
			 }
		 }
		 return i;
	}
	
}
