package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import javax.swing.JOptionPane;
import model.EquipementModel;
import view.EquipementView;

public class EquipementController implements ActionListener, WindowListener {
	private EquipementModel model; 
	private EquipementView view;
	private HashMap<String, String> createdEq = new HashMap<String, String>();
	private EquipementModel em;
	
	/*
	 * Constructeur
	 */
	public EquipementController(EquipementModel model, EquipementView view) {
		this.model = model;
		this.view = view;
	}

	/*
	 * Gestion des �v�nements sur les boutons(non-Javadoc)
	 */
	public void actionPerformed(ActionEvent arg0) {
		Object  source=arg0.getSource();

		//Selection du bouton qui a une action
		if (source == this.view.getBtView()) {
			this.view.checkEditable(false); //unlock equipement
			this.view.equipementDisplay("view");
		
		} else if(source == this.view.getBtEdit()) {
			Boolean isNotEditable = this.view.checkEditable(true);
			if (!isNotEditable) {
				this.view.equipementDisplay("edit");
			} 
			
		} else if(source == this.view.getListTechClass()) {
			this.view.setTechnicalSupplement(this.view.getListTechClass().getSelectedItem().toString());
	    	//Change le focus selon les champs activ�s
	    	this.view.setFocus();
			
		} else if(source == this.view.getListAssigned()) {
			String nomEmployee = this.view.getListAssigned().getSelectedItem().toString();
			this.view.getJtfLocalisation().setText(this.model.getUserLocList().get(nomEmployee));
			this.view.getJtfSup().setText(this.model.getUserSupList().get(nomEmployee));
		
		} else if (source == this.view.getBtSave()) {
			String message = "";
			int noInventaire = 0;
			String numSerie = null;
			String nom = null;
			String designation = null;
			String localisation = null;
			Date miseService = null;
			Date echeance = null;
			String nomFournisseur = null;
			String nomEmploye = null;
			DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
			
			/*
			 * Test des champs
			*/
			
			//champ localisation
			if (this.view.getJtfLocalisation().getText().trim().isEmpty()) {
				localisation = "-";
			} else {
				localisation = this.view.getJtfLocalisation().getText();
			}
			
			// champ num�ro de s�rie
			if (this.view.getJtfSerialNum().getText().trim().isEmpty()) { 
				message += "Le champ Num�ro de s�rie doit �tre rempli \r\n";
			} else {
				this.createdEq.put("jtfSerialNum", this.view.getJtfSerialNum().getText());
				numSerie = this.view.getJtfSerialNum().getText();
			}
			
			// champ d�signation
			if (this.view.getJtfDesignation().getText().trim().isEmpty()) { 
				message += "Le champ D�signation doit �tre rempli \r\n";
			} else {
				this.createdEq.put("jtfDesignation", this.view.getJtfDesignation().getText());
				designation = this.view.getJtfDesignation().getText();
			}
			
			// champ date de r�ception
			if (this.view.getJtfEqu().getText().trim().isEmpty() || this.view.getJtfEqu().getText().equals("AAAA-MM-JJ")) { 
				message += "Le champ R�ception du mat�riel doit �tre rempli \r\n";
			} else {
				this.createdEq.put("jtfReception", this.view.getJtfEqu().getText());
				try {
					miseService = formatter.parse(this.view.getJtfEqu().getText());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			// champ date garantie
			if (this.view.getJtfAssu().getText().trim().isEmpty() || this.view.getJtfAssu().getText().equals("AAAA-MM-JJ")) { 
				message += "Le champ Garantie doit �tre rempli \r\n";
			} else {
				this.createdEq.put("jtfAssu", this.view.getJtfAssu().getText());
				this.createdEq.put("jtfReception", this.view.getJtfEqu().getText());
				try {
					echeance = formatter.parse(this.view.getJtfEqu().getText());
				} catch (ParseException e) {
					e.printStackTrace();
				}
			}
			
			// champ date inventaire physique
			if(this.view.getJtfPhysInv().getText().equals("") || this.view.getJtfPhysInv().getText().equals("AAAA-MM-JJ")) {
				this.createdEq.put("jtfPhysInv", "NULL");
			} else {
				this.createdEq.put("jtfPhysInv", this.view.getJtfPhysInv().getText());
			}
			
			// champ Employ�
			if (this.view.getListAssigned().getSelectedItem().toString().equals("S�lection")) { 
				this.createdEq.put("listAssigned", "NULL");
				nomEmploye = "-";
			} else {
				this.createdEq.put("listAssigned", this.model.getUserList().get(this.view.getListAssigned().getSelectedItem().toString()));
				nomEmploye = this.view.getListAssigned().getSelectedItem().toString();
			}

			
			// Id Fournisseur
			if (this.view.getListSupplier().getSelectedItem().toString().equals("S�lection")) { 
				message += "Un fournisseur doit �tre choisi \r\n";
			} else {
				this.createdEq.put("listSupplier", this.model.getSuppliersList().get(this.view.getListSupplier().getSelectedItem().toString()));
				nomFournisseur = this.view.getListSupplier().getSelectedItem().toString();
			}
		
			// champ NoCommande
			if (this.view.getJtfOrder().getText().trim().isEmpty()) { 
				message += "Le champ NoCommande doit �tre rempli \r\n";
			} else {
				this.createdEq.put("jtfOrder", this.view.getJtfOrder().getText());
			}
			
			// champ prix
			try  { 
				Float.parseFloat(this.view.getJtfCost().getText());
				this.createdEq.put("jtfCost", this.view.getJtfCost().getText());
			} catch (Exception e) {
				message += "Le champ Prix doit �tre un nombre \r\n";
			}
			
			// compl�ments techniques
			String classTech = this.model.getTechClassList().get(this.view.getListTechClass().getSelectedItem().toString()); // Id Classe	
			if (classTech == null) { 
				message += "Une classe doit �tre choisie \r\n";
			} else {
				this.createdEq.put("listTechClass", classTech);
				
				if (classTech.equals("1") || classTech.equals("2") || classTech.equals("3")) { //laptop, desktop, serveur
					this.createdEq.put("cbxDuplex", "0");
					this.createdEq.put("cbxColor", "0");	
					this.createdEq.put("listMaxSize", "NULL");
					this.createdEq.put("jtfSize", "NULL");	
					this.createdEq.put("jtfEqu", this.view.getJtfNameEqu().getText());
					nom = this.view.getJtfNameEqu().getText();
				
				} else if (classTech.equals("4")) { //moniteur
					this.createdEq.put("cbxDuplex", "0");
					this.createdEq.put("cbxColor", "0");	
					this.createdEq.put("listMaxSize", "NULL");
					this.createdEq.put("jtfEqu", this.view.getJtfNameEqu().getText());
					nom = this.view.getJtfNameEqu().getText();
					 // champ size
					if (this.view.getJtfSize().getText().equals("")) {
						this.createdEq.put("jtfSize", "NULL");
					} else {
						try  { 
							Float.parseFloat(this.view.getJtfSize().getText());
							this.createdEq.put("jtfSize", this.view.getJtfSize().getText());
						} catch (Exception e) {
							message += "Le champ Size doit �tre un nombre \r\n";
						}			
					}
				
				} else { //imprimante
					this.createdEq.put("jtfSize", "NULL");
					this.createdEq.put("jtfEqu", this.view.getJtfNamePrint().getText());
					nom = this.view.getJtfNamePrint().getText();
					//RectoVerso
					if (this.view.getCbxDuplex().isSelected()) { 
						this.createdEq.put("cbxDuplex", "1");	
					} else {
						this.createdEq.put("cbxDuplex", "0");	
					}
					 //Couleur
					if (this.view.getCbxColor().isSelected()) {
						this.createdEq.put("cbxColor", "1");	
					} else {
						this.createdEq.put("cbxColor", "0");	
					}
					//Format Max
					if (this.view.getListMaxSize().getSelectedItem().toString().equals("S�lection")) { 
						this.createdEq.put("listMaxSize", "NULL");
					} else {
						this.createdEq.put("listMaxSize", this.model.getFormatsList().get(this.view.getListMaxSize().getSelectedItem().toString()));
					}
				}
			}
	
			//Garantie
			this.createdEq.put("txtAssurance", this.view.getTxtAreaAssurance().getText());
			
			//Remarques
			this.createdEq.put("txtNote", this.view.getTxtAreaNote().getText());
			
			this.createdEq.put("modifEnCours", "0");
			
			/*
			 * Fin test des champs
			 */
			
			if (message.equals("")) {// pas d'erreur
				if (this.view.getJtfInv().getText().trim().isEmpty()) { //pas de num�ro d'inventaire donc c'est un insert
					int result = this.model.insertEquipement(createdEq);
					if (result == 0) {
						JOptionPane.showMessageDialog(this.view, "L'�quipement n'as pas pu �tre enregistr�");
					
					} else {
						noInventaire = result;
						em = new EquipementModel(noInventaire, nomFournisseur, numSerie, nom, designation, nomEmploye, localisation, miseService, echeance);		
						this.view.setVisible(false);
					}
					
				} else { //il existe un num�ro d'inventaire donc c'est un update	
					int rep = JOptionPane.showConfirmDialog(this.view, "Voulez-vous modifier cet enregistrement?", "Gestion IT - Fiche mat�riel", JOptionPane.YES_NO_OPTION);
					if (rep == 0) {
						int result = this.model.updateEquipement(createdEq, Integer.parseInt(view.getJtfInv().getText())); 	
						if (result == 0) {
							JOptionPane.showMessageDialog(this.view, "L'�quipement n'as pas pu �tre modifi�", "Gestion IT - Fiche mat�riel", JOptionPane.ERROR_MESSAGE);
						
						} else {	
							noInventaire = Integer.parseInt(this.view.getJtfInv().getText());
							em = new EquipementModel(noInventaire, nomFournisseur, numSerie, nom, designation, nomEmploye, localisation, miseService, echeance);	
							this.view.setVisible(false);	
							this.view.checkEditable(false); //d�verouille l'�quipement
						}
					}
				}	
			} else {
				JOptionPane.showMessageDialog(this.view, message, "Gestion IT - Fiche mat�riel", JOptionPane.ERROR_MESSAGE);
			}			
		}
	}
	
	/*
	 * Retourne l'�quipement
	 * @return: EquipementModel
	 */
	public EquipementModel getEquipement() {
		return this.em;
	}

	
	public void windowActivated(WindowEvent arg0) {}
	public void windowClosed(WindowEvent arg0) {}
	public void windowClosing(WindowEvent arg0) {
		this.view.checkEditable(false);	
	}
	public void windowDeactivated(WindowEvent arg0) {}
	public void windowDeiconified(WindowEvent arg0) {}
	public void windowIconified(WindowEvent arg0) {}
	public void windowOpened(WindowEvent arg0) {}
	

}
