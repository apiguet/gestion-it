package controller;

import helpers.SupplierTableExtend;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import view.SupplierView;
import model.SupplierModel;

public class SupplierController implements ActionListener, MouseListener {
	private SupplierModel model; 
	private SupplierView view;
	private Object id;
	private SupplierModel sm;
	
	public SupplierController(SupplierModel model, SupplierView view) {
		this.model = model;
		this.view = view;	
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		Object source=arg0.getSource();
		this.view.getBtAdd().setEnabled(true);

		//Selection du bouton qui a une action
		if (source == this.view.getBtSave()){
			this.view.supplierDisplay("save");
			
			// on r�cup�re les valeurs des champs valid�es
			this.getFields((Integer) this.id);
			
			if (sm != null) { // si un �quipement a bien �t� initialis�
				
				// on effectue l'ajout ou la modification dans la db
				int res = this.model.executeSupplier(sm); 
				
				if (res == 0) { // un probl�me est survenu
					JOptionPane.showMessageDialog(this.view, "La requ�te a �chou�e", "Gestion IT - Fiche fournisseur", JOptionPane.ERROR_MESSAGE);
					this.view.getBtAdd().setEnabled(false);
				} else { // On met � jour la liste des fournisseurs et le JTable
					ArrayList<SupplierModel> smList = this.model.getSuppliersList();
					JTable t = this.view.getTable();
					SupplierTableExtend model = (SupplierTableExtend) t.getModel();
					
					if ((Integer) this.id == 0) { //c'est un insert
						sm.setId(res);
						model.addSupplier(sm);

						this.view.clearFields();
						this.view.supplierDisplay("view");
						
						this.view.setButtonEnabled(false);
		
					} else { // c'est un update
						int index = getSupplierFromList(smList, (Integer) this.id);	 
						model.updateSupplier(index, sm);
					}	
				}
			}
			
		} else if(source == this.view.getBtAdd()){
			this.view.supplierDisplay("add");
			this.id = 0;
			this.view.clearFields();
			this.view.getBtAdd().setEnabled(false);
			
		} else if(source == this.view.getBtEdit()){
			this.view.supplierDisplay("edit");
			this.view.displayFields((Integer) id);
			
		} else if(source == this.view.getBtDelete()){
			this.view.supplierDisplay("delete");
			
			int rep = JOptionPane.showConfirmDialog(this.view, "Etes-vous s�r de vouloir supprimer ce fournisseur?", "Gestion IT - fiche fournisseur", JOptionPane.YES_NO_OPTION);
			if (rep == 0) {
				int res = this.model.deleteSupplier((Integer) this.id);
				if (res == 0 || res == 1451) { // une erreur sql est survenue
					JOptionPane.showMessageDialog(this.view, "Au moins un �quipement est li� � ce fournisseur", "Gestion IT -  Fiche fournisseur", JOptionPane.ERROR_MESSAGE);		
			        this.view.supplierDisplay("view");
				
				} else { // on met � jour la liste des fournisseurs et le JTable
					ArrayList<SupplierModel> smList = this.model.getSuppliersList();			
					int index = getSupplierFromList(smList, (Integer) this.id);	
					JTable t = this.view.getTable();
					SupplierTableExtend model = (SupplierTableExtend) t.getModel();
					model.removeSupplier(index);
					
					this.view.clearFields();
			        this.view.supplierDisplay("view");
			        this.view.setButtonEnabled(false);
				}
				
			} else {
				this.view.supplierDisplay("view");
			}
		}
	}

	//Lignes s�lectionn�es dans le tableau de fournisseurs
	@Override
	public void mouseClicked(MouseEvent arg0) {
		this.view.getBtAdd().setEnabled(true);
		this.view.setButtonEnabled(true);

		JTable table = this.view.getTable();
        Point p = arg0.getPoint();
        int row = table.rowAtPoint(p);

       
        table.getValueAt(row, 0);
        this.id = table.getValueAt(row, 0);
        this.view.clearFields();
        this.view.supplierDisplay("view");
        
		//Test double clique
		if(arg0.getClickCount() == 2){
			this.view.displayFields((Integer) id);
		}
	}

	public void mouseEntered(MouseEvent arg0) {}
	public void mouseExited(MouseEvent arg0) {}
	public void mousePressed(MouseEvent arg0) {}
	public void mouseReleased(MouseEvent arg0) {}
	
	
	/*
	 * R�cup�re et teste les valeurs des champs
	 * @param: int, id du fournisseur
	 */
	public void getFields(int i) {
		String message = "";
		String raisonSociale = null;
		String ville =  null;
		String pays = null;
		int no = 0;
		int npa = 0;
		
		// champ RaisonSociale
		if (this.view.getJtfCompanyName().getText().trim().isEmpty()) { 
			message += "Le champ Raison sociale doit �tre rempli \r\n";
		} else {
			raisonSociale = this.view.getJtfCompanyName().getText();
		}
		
		// champ rue
		String rue = this.view.getJtfRoad().getText();
		
		//champ num�ro 
		if (!this.view.getJtfNumber().getText().trim().isEmpty()) { 
			try {
				no = Integer.parseInt(this.view.getJtfNumber().getText());
			} catch (Exception e) {
				message += "Le champ Num�ro doit �tre un nombre \r\n";
			}
		}
		
		// champ npa
		if (this.view.getJtfNPA().getText().trim().isEmpty()) {
			message += "Le champ NPA doit �tre rempli \r\n";
		}
		else {
			try {
				npa = Integer.parseInt(this.view.getJtfNPA().getText());
			} catch (Exception e) {
				message += "Le champ NPA doit �tre un nombre \r\n";
			}
		}
		
		// champ Ville
		if (this.view.getJtfCity().getText().trim().isEmpty()) { 
			message += "Le champ Ville doit �tre rempli \r\n";
		} else {
			ville = this.view.getJtfCity().getText();
		}
				
		// champ Pays
		if (this.view.getJtfCountry().getText().trim().isEmpty()) { 
			message += "Le champ Pays doit �tre rempli \r\n";
		} else {
			pays = this.view.getJtfCountry().getText();
		}
		
		// chamo t�l�phone
		String tel = this.view.getJtfPhone().getText();
		
		// champ email
		String email = this.view.getJtfEmail().getText();
		
		
		if (message.equals("")) { // pas d'erreur
			this.view.getBtAdd().setEnabled(true);
			sm = new SupplierModel(i, raisonSociale, rue, no, npa, ville, pays, tel, email);
		} else {
			sm = null;
			this.view.getBtAdd().setEnabled(false);
			JOptionPane.showMessageDialog(this.view, message, "Gestion IT - Fiche fournisseur", JOptionPane.ERROR_MESSAGE);		
		}	
	}
	
	// on r�cup�re l'id de la liste qui correspond � notre id Fournisseur
	public int getSupplierFromList(ArrayList<SupplierModel> smList, int index) {
		 int i = 0;
		 for (SupplierModel l : smList) { 
			  if (l.getId() == index) {
				  i = smList.indexOf(l);
				  break;
			  }
		 }
		 return i;
	}

}
