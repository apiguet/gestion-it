import controller.*;
import model.*;
import view.*;
public class GestionIT {
	
	 public static void main(String[] args){	 	
		 //Lancement de la fen�tre principale
	 	MainModel model = new MainModel();
	 	MainView view = new MainView(model);
	 	@SuppressWarnings("unused")
		MainController controller = new MainController(model, view);	
	 }
}
