package model;

import helpers.Connect;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.sql.rowset.CachedRowSet;

public class EquipementModel {
	private int noInventaire;
	private String numSerie;
	private String nom;
	private String designation;
	private String localisation;
	private Date miseService;
	private Date echeance;
	private String nomFournisseur;
	private String nomAttribueA;
	
	private Connect connect = new Connect();
	
	private HashMap<String, String> userList = new HashMap<String, String>();
	private HashMap<String, String> userLocList = new HashMap<String, String>();
	private HashMap<String, String> userSupList = new HashMap<String, String>();
	
	private HashMap<String, String> techClassList = new HashMap<String, String>();
	private HashMap<String, String> suppliersList = new HashMap<String, String>();
	private HashMap<String, String> formatsList = new HashMap<String, String>();
	
	/*
	 * constructeur vide
	 */
	public EquipementModel () {}

	/*
	 * constructeur liste mainView
	 */
	public EquipementModel (int noInventaire,
							String nomFournisseur,
							String numSerie,
							String nom,
							String designation,
							String nomAttribueA,
							String localisation,
							Date miseService,
							Date echeance)
	{
		this.setNoInventaire(noInventaire);
		this.setNomFournisseur(nomFournisseur);
		this.setNumSerie(numSerie);
		this.setNom(nom);
		this.setDesignation(designation);
		this.nomAttribueA = nomAttribueA;
		this.setLocalisation(localisation);
		this.setMiseService(miseService);
		this.setEcheance(echeance);
	}
	
	
	/*
	 * Getters et setters
	 */
	public String getnomAttribueA() {
		return this.nomAttribueA;
	}

	public int getNoInventaire() {
		return this.noInventaire;
	}
	public void setNoInventaire(int noInventaire) {
		this.noInventaire = noInventaire;
	}

	public String getNomFournisseur() {
		return this.nomFournisseur;
	}
	public void setNomFournisseur(String nomFournisseur) {
		this.nomFournisseur = nomFournisseur;
	}

	public String getNumSerie() {
		return this.numSerie;
	}
	public void setNumSerie(String numSerie) {
		this.numSerie = numSerie;
	}

	public String getNom() {
		return this.nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getDesignation() {
		return this.designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getLocalisation() {
		return this.localisation;
	}
	public void setLocalisation(String localisation) {
		this.localisation = localisation;
	}

	public Date getMiseService() {
		return this.miseService;
	}
	public void setMiseService(Date miseService) {
		this.miseService = miseService;
	}

	public Date getEcheance() {
		return this.echeance;
	}
	public void setEcheance(Date echeance) {
		this.echeance = echeance;
	}



	/*
	 * V�rifie si l'�quipement est actuellement modifiable
	 * @param: int => Id de l'�quipement qu'on aimerait modifier
	 * @return: boolean
	 */
	public Boolean isEditable(int id) {
		CachedRowSet rs = this.connect.queryDB("SELECT ModifEnCours FROM equipement e WHERE e.NoInventaire="+id);
		Boolean isEditable = null;
		try {
			while (rs.next()) {		
				isEditable = Boolean.parseBoolean(rs.getString("ModifEnCours"));
			}				
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return isEditable;
	}
	
	/*
	 * Change l'�tat de modifiation d'un �quipement
	 * @param: int => id de l'�quipement voulu
	 * @param: int => val (0 => modifiable, 1 => bloque la modification)
	 */
	public void setNoEditable(int id, int val) {
		this.connect.updateDB("UPDATE `equipement` SET `ModifEnCours` = '"+val+"' WHERE `equipement`.`NoInventaire`="+id);
	}
	
	
	/*
	 * S�lection d'un �quipement dans la db selon son id
	 * @param: int => id de l'�quipement
	 * @return: HashMap<String, String>
	 */
	public ArrayList<HashMap<String, String>> selectEquipement(int id) {
		CachedRowSet rs = this.connect.queryDB("SELECT * FROM equipement e " +
				"LEFT OUTER JOIN utilisateur u1 ON u1.NoEmploye = e.NoEmploye " +
				"LEFT OUTER JOIN dirigeant d ON d.NoEmploye = u1.NoEmploye " +
				"LEFT OUTER JOIN utilisateur u2 ON u2.NoEmploye = d.NoEmplSuperieur " +
				"LEFT OUTER JOIN localisation l ON l.CodeLocal = u1.CodeLocal " +
				"LEFT OUTER JOIN fournisseur f ON f.IdFournisseur = e.IdFournisseur " +
				"LEFT OUTER JOIN classetechnique ct ON ct.IDClasse = e.IDClasse " +
				"LEFT OUTER JOIN formatimpression fi ON fi.IDFormat = e.FormatMax " + 
				"LEFT OUTER JOIN composant c ON c.NoInventaire = e.NoInventaire " +
				"LEFT OUTER JOIN comp_disque cd ON cd.IdComposant = c.IdComposant " +
				"LEFT OUTER JOIN comp_reseau cr ON cr.IdComposant = c.IdComposant " + 
				"WHERE e.NoInventaire="+id);

		ArrayList<HashMap<String,String>> rows = new ArrayList<HashMap<String,String>>();

		try {
			while (rs.next()) {	
				HashMap<String, String> fieldValues = new HashMap<String, String>();
				
				fieldValues.put("listTechClass", rs.getString(46));
				fieldValues.put("jtfInv", rs.getString("NoInventaire"));
				String nomEmploye = rs.getString(22)+" "+rs.getString(21);
				fieldValues.put("listAssigned", nomEmploye);
				fieldValues.put("jtfEqu", rs.getString("Nom"));
				fieldValues.put("jtfSerialNum", rs.getString("NoSerie"));
				fieldValues.put("jtfAssu", rs.getString("DFinGarantie"));
				fieldValues.put("jtfDesignation", rs.getString("Designation"));
				fieldValues.put("jtfPhysInv", rs.getString("DInventaire"));
				fieldValues.put("jtfReception", rs.getString("DReception"));
				
				fieldValues.put("listSupplier", rs.getString("RaisonSociale"));
				fieldValues.put("jtfLocalisation", rs.getString("NoLocal"));
				fieldValues.put("jtfOrder", rs.getString("NoCommande"));
				String nomSuperieur = rs.getString(30)+" "+rs.getString(29);
				fieldValues.put("jtfSup", nomSuperieur);
				fieldValues.put("jtfCost", rs.getString("Prix"));
				fieldValues.put("jtfNameEqu", rs.getString("Nom"));
				fieldValues.put("jtfSize", rs.getString("Taille"));
				fieldValues.put("jtfNamePrint", rs.getString("Nom"));
				fieldValues.put("cbxDuplex", rs.getString("RectoVerso"));
				fieldValues.put("listMaxSize", rs.getString("Libelle")); //48
				fieldValues.put("cbxColor", rs.getString("Couleur"));
				
				fieldValues.put("txtAeraNote", rs.getString("Remarque"));
				fieldValues.put("txtAeraAssurance", rs.getString("TxtGarantie"));	
				
				//composant
				fieldValues.put("composantOS", rs.getString("OS"));
				fieldValues.put("composantMem", rs.getString("Memoire"));
				fieldValues.put("composantCpuNb", rs.getString("CPUNb"));
				fieldValues.put("composantCpuType", rs.getString("CPUType"));
				fieldValues.put("composantCpuFreq", rs.getString("CPUFrequ"));
				fieldValues.put("composantBios", rs.getString("BIOSVer"));
				fieldValues.put("composantPath", rs.getString("Chemin"));	
				fieldValues.put("composantCap", rs.getString("Capacite"));
				fieldValues.put("composantLib", rs.getString(62));
				fieldValues.put("composantIp", rs.getString("IP"));
				
				rows.add(fieldValues);
			}
				
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		//return fieldValues;
		return rows;
	}
	
	/*
	 * Insertion d'un nouvel �quipement dans la db
	 * @param: HashMap<String, String>
	 * @return: int
	 */
	public int insertEquipement(HashMap<String, String> e) {
		return this.connect.executeEquipementDB(e, 0);
	}
	
	/*
	 * Update d'un �quipement dans la db
	 * @param: HashMap<String, String>
	 * @param: int 
	 * @return: int
	 */
	public int updateEquipement(HashMap<String, String> e, int id) {
		return this.connect.executeEquipementDB(e, id);
	}
	
		
	/*
	 * Retourne la liste des classes techniques 
	 * @return: String[]
	 */
	public String[] getTechClasses() {
		CachedRowSet rs = this.connect.queryDB("SELECT * FROM classetechnique");
		String[] techClass = new String[6];
		techClass[0] = "S�lection";
		try {
			while (rs.next()) {
				techClass[rs.getInt("IDClasse")] = rs.getString("Nom");
				techClassList.put(rs.getString("Nom"), rs.getString("IDClasse"));
			}
			
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return techClass;
	}
	
	/*
	 * Retourne la liste map�e classe technique-id
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getTechClassList() {
		return this.techClassList;
	}
	
	/*
	 * Retourne la liste des fournisseurs
	 * @return: String[]
	 */
	public String[] getSuppliers() {
		CachedRowSet crs = this.connect.queryDB("SELECT COUNT(*) AS C FROM fournisseur");
		int nb = 0;
		try {
			crs.next();
			nb = crs.getInt("C") + 1;
		} catch (SQLException e1) {
			e1.printStackTrace();
		}

		CachedRowSet rs = this.connect.queryDB("SELECT * FROM fournisseur");
		String[] suppliers = new String[nb];
		suppliers[0] = "S�lection";
		int i = 1;
		try {
			while (rs.next()) {
				suppliers[i] = rs.getString("RaisonSociale");
				this.suppliersList.put(rs.getString("RaisonSociale"), rs.getString("IDFournisseur"));
				i++;
			}		
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return suppliers;
	}
	
	/*
	 * Retourne la liste map�e fournisseur-id
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getSuppliersList() {
		return this.suppliersList;
	}
		
	/*
	 * Retourne la liste des tailles maximum
	 * @return: String[]
	 */
	public String[] getMaxSize() {
		CachedRowSet rs = this.connect.queryDB("SELECT * FROM formatimpression");
		String[] formats = new String[5];
		formats[0] = "S�lection";
		try {
			while (rs.next()) {
				formats[rs.getInt("IDFormat")] = rs.getString("libelle");
				this.formatsList.put(rs.getString("libelle"), rs.getString("IDFormat"));
			}	
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return formats;
	}
	
	/*
	 * Retourne la liste map�e format-id
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getFormatsList() {
		return this.formatsList;
	}
	

	/*
	 * Retoune une liste des employ�s pour le combo
	 * + une list map�e id-nom
	 * + une liste map�e nom-sup
	 * + une liste map�e nom-localisation
	 * @return: String[]
	 */
	public String[] getAssigned() {
		CachedRowSet rs = this.connect.queryDB("SELECT u.NoEmploye, u.Nom, u.Prenom, NoLocal, u2.Nom, u2.Prenom FROM utilisateur u " +
				"LEFT OUTER JOIN localisation l ON l.CodeLocal = u.CodeLocal " +
				"LEFT OUTER JOIN dirigeant d ON d.NoEmploye = u.NoEmploye "  +
				"LEFT OUTER JOIN utilisateur u2 ON u2.NoEmploye = d.NoEmplSuperieur");
		String[] users = new String[41];
		users[0] = "S�lection";
		try {
			while (rs.next()) {
				String nomEmploye = rs.getString("Prenom")+" "+rs.getString("Nom");
				String nomDirigeant = rs.getString(6)+" "+rs.getString(5);
				users[rs.getInt("NoEmploye")] = nomEmploye;
				this.userList.put(nomEmploye, rs.getString("NoEmploye"));
				this.userLocList.put(nomEmploye, rs.getString("NoLocal"));
				this.userSupList.put(nomEmploye, nomDirigeant);
			}	
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return users;
	}
	
	/*
	 * retourne la liste des users nom-id
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getUserList() {
		return this.userList;
	}
	
	/*
	 * retourne la liste des users employe-localisation
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getUserLocList() {
		return this.userLocList;
	}
		
	/*
	 * retourne la liste des users employe-dirigeant
	 * @return: HashMap<String, String>
	 */
	public HashMap<String, String> getUserSupList() {
		return this.userSupList;
	}
		
}
