package model;

import helpers.Connect;
import java.util.ArrayList;
import javax.sql.rowset.CachedRowSet;

public class SupplierModel {
	private int id;
	private String raisonSociale;
	private String rue;
	private int no;
	private int npa;
	private String ville;
	private String pays;
	private String tel;
	private String email;
	
	private ArrayList<SupplierModel> supplierList = new ArrayList<SupplierModel>();	
	private Connect connect = new Connect();
	
	//Constructeurs
	public SupplierModel() {
		this.selectSuppliersList();
	}
	
	public SupplierModel (int id,
					String raisonSociale,
					String rue,
					int no,
					int npa,
					String ville,
					String pays,
					String tel,
					String email) {
		
		this.setId(id);
		this.setRaisonSociale(raisonSociale);
		this.setRue(rue);
		this.setNo(no);
		this.setNpa(npa);
		this.setVille(ville);
		this.setPays(pays);
		this.setTel(tel);
		this.setEmail(email);
	}
	
	/*
	 * S�lection de tous les fournisseurs
	 */
	public void selectSuppliersList() {
		CachedRowSet rs = this.connect.queryDB("SELECT * FROM fournisseur");
		try {
			while (rs.next()) {
				int id = rs.getInt("IDFournisseur");
				String raisonSociale = rs.getString("RaisonSociale");
				String rue = rs.getString("Rue"); 	
				int no = rs.getInt("NoRue");	
				int npa = rs.getInt("NPA"); 	
				String ville = rs.getString("Ville"); 	
				String pays = rs.getString("Pays");	
				String tel = rs.getString("Tel"); 	
				String email = rs.getString("Email");
				
				this.supplierList.add(new SupplierModel(id, raisonSociale, rue, no, npa, ville, pays, tel, email));
			}
		} catch (Exception e) {
			System.out.println("Erreur Fournisseur !");
		}	
	}
	
	/*
	 * Retourne la liste des fournisseurs
	 * @return: ArrayList<SupplierModel>
	 */
	public ArrayList<SupplierModel> getSuppliersList() {
		return this.supplierList;
	}

	
	/*
	 * Ajoute ou modifie un fournisseur
	 * @return: int (0 si la requ�te s'est mal pass�e, 1 s'il s'agit d'un update ou l'id cr�� s'il s'agit l'insert)
	 * @param: objet fournisseur
	 */
	public int executeSupplier(SupplierModel sm) {
		int rs = this.connect.executeSupplierDB(sm);
		return rs;
	}
	
	/*
	 * Supprime un fournisseur suivant sont id
	 * @return: int (0 si la requ�te s'est mal pass�e, 1451 si le fournisseur ne peut pas �tre supprim�, 1 sinon)
	 * @param: id du fournisseur
	 */
	public int deleteSupplier(int id) {
		int nbRecords = 0;
		try {
			nbRecords = this.connect.deleteSupplierDB(id);
		} catch (Exception e) {}
		return nbRecords;
	}
	
	
	// Getters et setters
	public int getId() {
		return this.id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getRaisonSociale() {
		return this.raisonSociale;
	}
	public void setRaisonSociale(String raisonSociale) {
		this.raisonSociale = raisonSociale;
	}
	
	public String getRue() {
		return this.rue;
	}
	public void setRue(String rue) {
		this.rue = rue;
	}

	public int getNo() {
		return this.no;
	}
	public void setNo(int no) {
		this.no = no;
	}

	public int getNpa() {
		return this.npa;
	}
	public void setNpa(int npa) {
		this.npa = npa;
	}

	public String getVille() {
		return this.ville;
	}
	public void setVille(String ville) {
		this.ville = ville;
	}

	public String getPays() {
		return this.pays;
	}
	public void setPays(String pays) {
		this.pays = pays;
	}

	public String getTel() {
		return this.tel;
	}
	public void setTel(String tel) {
		this.tel = tel;
	}

	public String getEmail() {
		return this.email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
}
