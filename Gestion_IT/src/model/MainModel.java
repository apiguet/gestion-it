package model;

import helpers.Connect;
import java.util.ArrayList;
import java.util.Date;
import javax.sql.rowset.CachedRowSet;


public class MainModel {
	private ArrayList<EquipementModel> emSL = new ArrayList<EquipementModel>(); //liste avec les �quipements en stock
	private ArrayList<EquipementModel> emAL = new ArrayList<EquipementModel>();	//liste avec les �quipements assign�s
	private Connect connect = new Connect();

	/*
	 * Constructeur du model
	 */
	public MainModel(){
		this.getEquipementList();
	}
	
	/*
	 * Cr�ation des listes des �quipements assign�s ou en stock
	 */
	public void getEquipementList() {

		CachedRowSet rs = connect.queryDB("SELECT * FROM equipement e " +
				"LEFT OUTER JOIN utilisateur u ON u.NoEmploye = e.NoEmploye " +
				"LEFT OUTER JOIN localisation l ON l.CodeLocal = u.CodeLocal " +
				"LEFT OUTER JOIN fournisseur f ON f.IdFournisseur = e.IdFournisseur");
		
		try {
			while (rs.next()) {

				int noInventaire = rs.getInt("NoInventaire");
				String nomFournisseur = rs.getString("RaisonSociale");
				String numSerie = rs.getString("NoSerie");
				String nom = rs.getString("Nom");
				String designation = rs.getString("Designation");
				String nomEmploye = rs.getString(22) + " " + rs.getString(21);
				String localisation = rs.getString("NoLocal");
				Date miseService = rs.getDate("DReception");
				Date echeance = rs.getDate("DFinGarantie");
				
				EquipementModel emList;
				
				//Cr�er les listes des equipement (avec ou sans nom d'utilisateur attribu�)
				if (nomEmploye.startsWith("null")) {
					emList = new EquipementModel(noInventaire, nomFournisseur, numSerie, nom, designation, "-", "-", miseService, echeance);
					this.emSL.add(emList);
				} else {
					emList = new EquipementModel(noInventaire, nomFournisseur, numSerie, nom, designation, nomEmploye, localisation, miseService, echeance);
					this.emAL.add(emList);
				}			
			}
		} catch (Exception e) {
			System.out.println("Erreur !");
		}	
	}
	
	/*
	 * Retourne la liste des �quipements en stock
	 * @return: ArrayList<EquipementModel>
	 */
	public ArrayList<EquipementModel> getEquipementStockList() {
		return this.emSL;
	}

	/*
	 * Retourne la liste des �quipements assign�s
	 * @return: ArrayList<EquipementModel>
	 */
	public ArrayList<EquipementModel> getEquipementAssignedList() {
		return this.emAL;
	}
	

	/*
	 * Suppression d'un �quipement donn�
	 * @param: int => id de l'�quipement � supprimer
	 * @return: int
	 */
	public int deleteEquipement(int id) {
		int rs = this.connect.deleteEquipementDB(id);
		return rs;
	}
	
	/*
	 * V�rifie si un �quipement est �ditable et n'est pas d�j� en cours d'�dition
	 * @param: int => id de l'�quipement
	 * @return: boolean
	 */
	public Boolean isEditable(int id) {
		CachedRowSet rs = this.connect.queryDB("SELECT ModifEnCours FROM equipement e WHERE e.NoInventaire="+id);
		Boolean isEditable = null;
		try {
			while (rs.next()) {		
				isEditable = Boolean.parseBoolean(rs.getString("ModifEnCours"));
			}				
		} catch (Exception e) {
			System.out.println("Erreur !");
		}
		return isEditable;
	}
	
}
